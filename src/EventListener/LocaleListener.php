<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 16.04.19
 * Time: 16:02.
 */

namespace App\EventListener;

use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * @codeCoverageIgnore
 * Class LocaleListener
 */
class LocaleListener
{
    public function onKernelRequest(RequestEvent $responseEvent)
    {
        setlocale(LC_ALL, 'de_CH.utf-8');
    }

    /**
     * @param GetResponseEvent $responseEvent
     */
    public function onConsoleCommand(ConsoleCommandEvent $responseEvent)
    {
        setlocale(LC_ALL, 'de_CH.utf-8');
    }
}
