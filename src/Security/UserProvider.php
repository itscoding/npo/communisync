<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    private $adminPassword;

    public function __construct(string $adminPassword)
    {
        $this->adminPassword = $adminPassword;
    }

    public function loadUserByUsername(string $username)
    {
        $user = new User();
        $user->setPassword($this->adminPassword);
        $user->setUsername('admin');

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        return $user;
    }

    public function supportsClass(string $class)
    {
        return true;
    }

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
    }
}
