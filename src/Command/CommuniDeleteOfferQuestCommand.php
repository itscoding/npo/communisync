<?php

namespace App\Command;

use App\Services\Communi\Offer\OfferQuestRemover;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @codeCoverageIgnore
 */
class CommuniDeleteOfferQuestCommand extends Command
{
    protected static $defaultName = 'communi:delete:offer-quests';
    private OfferQuestRemover $offerQuestRemover;

    /**
     * CommuniSyncEventsCommand constructor.
     */
    public function __construct(
        OfferQuestRemover $offerQuestRemover
    ) {
        parent::__construct();
        $this->offerQuestRemover = $offerQuestRemover;
    }

    /**
     * Command Setup.
     */
    protected function configure()
    {
        $this
            ->setDescription('Delete Offers older then `days` day old')
            ->addArgument('days-ago', InputArgument::REQUIRED, 'delete offers X `days` ag')
            ->addArgument('official', InputArgument::OPTIONAL, 'delete only: official [1] / non official [1] offers / all if is not set');
    }

    /**
     * @return int|void|null
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $style = new SymfonyStyle($input, $output);
        $style->write('Starting to Delete OfferQuests');
        $style->newLine(2);
        $daysAgo = $input->getArgument('days-ago');
        $official = $input->getArgument('official');

        if ($daysAgo > 999) {
            $style->error('max value for days-ago is 999');

            return 100;
        }
        $deletedOfferQuests = $this->offerQuestRemover->removeOffersAndQuests($daysAgo, $official);
        $style->success(sprintf('Deleted %s OfferQuests', count($deletedOfferQuests)));

        return 0;
    }
}
