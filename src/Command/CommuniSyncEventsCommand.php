<?php

namespace App\Command;

use App\Services\Synchronizer\EventSynchronizer;
use App\Services\Synchronizer\EventSynchronizerFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @codeCoverageIgnore
 * Class CommuniSyncEventsCommand.
 */
class CommuniSyncEventsCommand extends Command
{
    protected static $defaultName = 'communi:sync:events';

    private EventSynchronizer $eventSynchronizer;

    /**
     * CommuniSyncEventsCommand constructor.
     */
    public function __construct(
        EventSynchronizerFactory $eventSynchronizerFactory
    ) {
        $this->eventSynchronizer = $eventSynchronizerFactory->createSynchronizer();
        parent::__construct();
    }

    /**
     * Command Setup.
     */
    protected function configure()
    {
        $this
            ->setDescription('Get The Events from all Sources and POST/PUT them to the communiapp')
            ->addArgument('--dry-run', InputArgument::OPTIONAL, 'do not post the data to communiapp');
    }

    /**
     * @return int|void|null
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $style = new SymfonyStyle($input, $output);
        $style->write('Start Synchronize Task');
        $style->newLine(2);
        $events = $this->eventSynchronizer->getCollectedEvents();

        if ($input->getArgument('--dry-run')) {
            $style->info(json_encode($events, JSON_PRETTY_PRINT));
            $style->success(sprintf('found %s Communi-Events', count($events)));

            return 0;
        }

        $eventCount = count($events);
        $progress = $style->createProgressBar($eventCount);
        $start = time();
        $this->eventSynchronizer->synchronize($events, $progress);
        $end = time();

        $seconds = $end - $start;
        $usedTime = gmdate('i:s', $seconds);
        $style->newLine(2);
        $style->success(sprintf(
            'Found %s "Source-Events", created %d "Communi-Events", updated %d "Communi-Events": in %s min',
            $eventCount,
            $this->eventSynchronizer->getCreated(),
            $this->eventSynchronizer->getUpdated(),
            $usedTime
        ));

        return 0;
    }
}
