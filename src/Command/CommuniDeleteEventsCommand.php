<?php

namespace App\Command;

use App\Repository\EventSyncRepository;
use App\Services\Communi\Event\EventRequestExecutor;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * @codeCoverageIgnore
 * Class CommuniDeleteEventsCommand.
 */
class CommuniDeleteEventsCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'communi:delete:events';
    private EventRequestExecutor $executor;
    private EventSyncRepository $syncRepository;

    /**
     * CommuniDeleteEventsCommand constructor.
     */
    public function __construct(
        EventRequestExecutor $executor,
        EventSyncRepository $syncRepository,
        LoggerInterface $logger
    ) {
        $this->executor = $executor;
        $this->syncRepository = $syncRepository;
        parent::__construct();
        $this->logger = $logger;
    }

    /**
     * Command Setup.
     */
    protected function configure()
    {
        $this
            ->setDescription('Delete multiple or single events on the communiapp created with communisync')
            ->addOption('force')
            ->addOption('all')
            ->addArgument('id', InputArgument::OPTIONAL, 'remove by communiapp id');
    }

    /**
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        if (!$input->getOption('force')) {
            $io->error('you must add the --force flag to execute this');

            return;
        }
        $start = time();
        if ($input->getOption('all')) {
            $eventSyncs = $this->syncRepository->findAll();
            $count = 0;
            $errors = 0;
            $progress = $io->createProgressBar(count($eventSyncs));
            foreach ($eventSyncs as $sync) {
                try {
                    $this->executeDelete((int) $sync->getCommuniEventId());
                    ++$count;
                } catch (\Exception $exception) {
                    $this->syncRepository->removeByCommuniId($sync->getCommuniEventId());
                    ++$errors;
                    $this->logger->info($exception->getMessage());
                }

                $progress->advance();
            }
            $end = time();
            $seconts = $end - $start;
            $usedTime = gmdate('i:s', $seconts);
            $io->success(sprintf('Removed %s Events, %s could not be deleted: in %s min', $count, $errors, $usedTime));

            return;
        } elseif ($input->getArgument('id')) {
            $id = (int) $input->getArgument('id');
            $deleted = $this->executeDelete($id);
            if ($deleted) {
                $io->success(sprintf('Removed Event with id %s', $id));

                return;
            }
            $io->success(sprintf('Could not delete Event with id %s', $id));

            return;
        }
        $io->error('You must provide an id or the --all flag');
    }

    public function executeDelete(int $id)
    {
        $this->executor->deleteEvent($id);
        $this->syncRepository->removeByCommuniId($id);
    }
}
