<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 21:56.
 */

namespace App\Hydrators;

use App\Entity\Event;
use App\Services\Kool\HydratorStrategies\DescriptionStrategy;
use Laminas\Hydrator\ClassMethodsHydrator;

/**
 * Class EventHydrator.
 */
class EventHydrator extends ClassMethodsHydrator
{
    public function hydrate(array $data, object $object): Event
    {
        if (array_key_exists('description', $data)) {
            $data['description'] = DescriptionStrategy::decodeBreaks($data['description']);
            $data['title'] = $data['description'];
            $data['summarySlug'] = $data['description'];
            $data['slug'] = $data['summary'];
        }

        /** @var Event $event */
        $event = parent::hydrate($data, $object);

        return $event;
    }

    public function extract(object $object): array
    {
        return parent::extract($object);
    }
}
