<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 21.02.19
 * Time: 14:36.
 */

namespace App\Hydrators\Strategies;

use App\Services\Slugger\Utf8Slugger;
use Laminas\Hydrator\Strategy\StrategyInterface;

/**
 * Class SlugStrategy.
 */
class SlugStrategy implements StrategyInterface
{
    private Utf8Slugger $slugger;

    /**
     * SlugStrategy constructor.
     */
    public function __construct(Utf8Slugger $slugger)
    {
        $this->slugger = $slugger;
    }

    /**
     * @param mixed $value
     *
     * @return mixed|void
     */
    public function extract($value, ?object $object = null)
    {
        return $value;
    }

    /**
     * @param mixed $value
     *
     * @return string
     */
    public function hydrate($value, ?array $data)
    {
        return $this->slugger->slugify($data['summary']);
    }
}
