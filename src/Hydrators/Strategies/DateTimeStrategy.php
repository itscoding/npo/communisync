<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 22:15.
 */

namespace App\Hydrators\Strategies;

use App\Entity\Event;
use App\Services\Communi\Event\EventParser;
use Laminas\Hydrator\Strategy\StrategyInterface;

/**
 * Class DateTimeStrategy.
 */
class DateTimeStrategy implements StrategyInterface
{
    private ?string $timezone;

    /**
     * DateTimeStrategy constructor.
     *
     * @param string $timezone
     */
    public function __construct(string $timezone = null)
    {
        $this->timezone = $timezone;
    }

    /**
     * @param string     $value
     * @param Event|null $object
     *
     * @return string
     *
     * @throws \Exception
     */
    public function extract($value, ?object $event = null)
    {
        if ($event) {
            return $event->getDateTime()->format(EventParser::DATE_FORMAT);
        }

        return $value;
    }

    /**
     * @param string $value
     *
     * @return \DateTime
     *
     * @throws \Exception
     */
    public function hydrate($value, ?array $data)
    {
        $dateTime = new \DateTime($value);
        if ($this->timezone) {
            $dateTime->setTimezone(new \DateTimeZone($this->timezone));
        }

        return $dateTime;
    }
}
