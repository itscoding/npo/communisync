<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 01.02.19
 * Time: 21:26.
 */

namespace App\Hydrators\Strategies;

use Laminas\Hydrator\Strategy\StrategyInterface;

/**
 * Class HtmlEncodeStrategy.
 */
class HtmlEncodeStrategy implements StrategyInterface
{
    public function extract($value, ?object $event = null)
    {
        if ($event) {
            return 'could not be hydrated';
        }

        return htmlentities($value, ENT_QUOTES, 'UTF-8');
        //return htmlspecialchars($value);
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public function hydrate($value, ?array $data)
    {
        return htmlspecialchars_decode($value);
    }
}
