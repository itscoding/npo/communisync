<?php

namespace App\Repository;

use App\Entity\EventSync;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @codeCoverageIgnore
 * Class EventSyncRepository.
 */
class EventSyncRepository extends ServiceEntityRepository
{
    /**
     * EventSyncRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventSync::class);
    }

    public function findBySourceId(string $sourceId): ?EventSync
    {
        return $this->findOneBy(['sourceEventId' => $sourceId]);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function storeEventSync(EventSync $eventSync)
    {
        $this->getEntityManager()->persist($eventSync);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @return array|EventSync[]
     */
    public function findWhereIdIn(array $syncIds): array
    {
        $statement = $this->createQueryBuilder('es')
            ->select('es')
            ->where('es.sourceEventId IN (:syncIds)')
            ->setParameter(':syncIds', $syncIds);

        return $statement->getQuery()->execute();
    }

    /**
     * @return array|EventSync[]
     */
    public function findAll(): array
    {
        return parent::findAll();
    }

    /**
     * @return object|null
     */
    public function findByCommuniId(int $id)
    {
        return $this->findOneBy(['communiEventId' => $id]);
    }

    public function removeByCommuniId(int $id)
    {
        $entity = $this->findByCommuniId($id);
        if ($entity) {
            $this->getEntityManager()->remove($entity);
            $this->flush();
        }
    }
}
