<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 19.03.19
 * Time: 17:26.
 */

namespace App\Repository;

use App\Entity\EventType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class EventTypeRepository.
 */
class EventTypeRepository extends ServiceEntityRepository
{
    /**
     * EventTypeRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventType::class);
    }

    /**
     * @return array|EventType[]
     */
    public function findAll(): array
    {
        return parent::findAll();
    }

    public function persist(EventType $eventType)
    {
        $this->getEntityManager()->persist($eventType);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }
}
