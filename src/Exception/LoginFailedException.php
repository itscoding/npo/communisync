<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 19:46.
 */

namespace App\Exception;

/**
 * Class LoginFailedException.
 */
class LoginFailedException extends \Exception
{
}
