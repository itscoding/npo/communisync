<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 01.02.19
 * Time: 20:59.
 */

namespace App\Exception;

/**
 * Class NotUpdateableException.
 */
class NotUpdateableException extends \Exception
{
}
