<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 07.02.19
 * Time: 23:23.
 */

namespace App\Exception;

/**
 * Class EventAlreadyExistsException.
 */
class EventAlreadyExistsException extends \Exception
{
}
