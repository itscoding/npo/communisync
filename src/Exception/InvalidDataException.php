<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 10:40.
 */

namespace App\Exception;

/**
 * Class InvalidDataException.
 */
class InvalidDataException extends \Exception
{
}
