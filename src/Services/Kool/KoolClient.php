<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 23:32.
 */

namespace App\Services\Kool;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * @codeCoverageIgnore
 * Class KoolClient.
 */
class KoolClient
{
    const CALENDAR_URI = '/ical';
    private string $userHash;
    private string $egsParameter;

    /**
     * KoolClient constructor.
     */
    public function __construct(
        string $koolUrl,
        string $userHash,
        string $egsParameter,
        Client $client = null
    ) {
        $this->client = $client ?: new Client([
            'base_uri' => ltrim($koolUrl, '/'),
        ]);

        $this->userHash = $userHash;
        $this->egsParameter = $egsParameter;
    }

    public function getICal(): ResponseInterface
    {
        return $this->client->get(self::CALENDAR_URI, [
            'query' => [
                'user' => $this->userHash,
                'egs' => $this->egsParameter,
            ],
        ]);
    }
}
