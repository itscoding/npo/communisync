<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 23:31.
 */

namespace App\Services\Kool\Event;

use App\Entity\Event;
use App\Services\Kool\KoolClient;

/**
 * @codeCoverageIgnore
 * Class EventRequestExecutor.
 */
class EventRequestExecutor
{
    private KoolClient $koolClient;
    private EventParser $eventParser;

    /**
     * EventRequestExecutor constructor.
     */
    public function __construct(
        KoolClient $koolClient,
        EventParser $eventParser
    ) {
        $this->koolClient = $koolClient;
        $this->eventParser = $eventParser;
    }

    /**
     * @return array|Event[]
     */
    public function getEvents()
    {
        $response = $this->koolClient->getICal();

        return $this->eventParser->parseResponse($response);
    }
}
