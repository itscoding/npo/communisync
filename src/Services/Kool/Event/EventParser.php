<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 23:32.
 */

namespace App\Services\Kool\Event;

use App\Entity\Event;
use App\Hydrators\Strategies\DateTimeStrategy;
use App\Hydrators\Strategies\HtmlEncodeStrategy;
use App\Hydrators\Strategies\SlugStrategy;
use App\Services\Kool\HydratorStrategies\DescriptionCriteria;
use App\Services\Kool\HydratorStrategies\DescriptionStrategy;
use App\Services\Kool\HydratorStrategies\Formatter\ContentFormatter;
use App\Services\Kool\HydratorStrategies\Formatter\SlugFormatter;
use App\Services\Kool\HydratorStrategies\Formatter\TextFormatter;
use App\Services\Slugger\Utf8Slugger;
use App\Services\SourceEvents\SourceEventParser;
use ICal\Event as ICalEvent;
use ICal\ICal;
use Psr\Http\Message\ResponseInterface;
use Zend\Hydrator\NamingStrategy\MapNamingStrategy;

/**
 * Class EventParser.
 */
class EventParser extends SourceEventParser
{
    /**
     * Max Events to Load.
     */
    const MAX_EVENTS = 112;

    private string $timeZone = 'Europe/Zurich';
    private int $filterDaysAfter = 999;
    private int $filterDaysBefore = 0;
    private array $nameMapping = [
        'uid' => 'sourceEventId',
        'dtstart' => 'dateTime',
        'dtend' => 'endDateTime',
    ];

    private ICal $iCal;

    public function setICal(ICal $iCal): void
    {
        $this->iCal = $iCal;
    }

    public function setFilterDaysAfter(int $filterDaysAfter): void
    {
        $this->filterDaysAfter = $filterDaysAfter;
    }

    public function setFilterDaysBefore(int $filterDaysBefore): void
    {
        $this->filterDaysBefore = $filterDaysBefore;
    }

    /**
     * @return array|Event[]
     */
    protected function responseToCommuniEvents(ResponseInterface $response): array
    {
        $iCal = $response->getBody()->getContents();

        //Outlook will interpret the Line with "X-ALT-DESC;", in communisync we use the "X-ALT-DESC:"
        //Comment By [Renzo Lauper, Stefan Müller, Simon Müller] @ 30.03.2019
        $iCal = str_replace('X-ALT-DESC;', 'X-ALT-DESC:', $iCal);

        if (!isset($this->iCal)) {
            $this->iCal = new ICal($iCal, [
                'disableCharacterReplacement' => true,
                'filterDaysBefore' => $this->filterDaysBefore,
                'filterDaysAfter' => $this->filterDaysAfter,
            ]);
        }

        $communiEvents = [];
        /**         * @var $event ICalEvent */
        foreach ($this->iCal->events() as $event) {
            $communiEvents[$event->uid] = $this->eventHydrator->hydrate(
                (array) $event,
                new Event()
            );
        }

        return array_slice($communiEvents, 0, self::MAX_EVENTS);
    }

    /**
     * Add the Hydration Strategies.
     */
    protected function setHydratorStrategies(): void
    {
        $dateTimeStrategy = new DateTimeStrategy($this->timeZone);
        $this->eventHydrator->setNamingStrategy(MapNamingStrategy::createfromHydrationMap($this->nameMapping));
        $this->eventHydrator->addStrategy('slug', new SlugStrategy(new Utf8Slugger()));
        $this->eventHydrator->addStrategy('dateTime', $dateTimeStrategy);
        $this->eventHydrator->addStrategy('endDateTime', $dateTimeStrategy);
        $this->eventHydrator->addStrategy('location', new HtmlEncodeStrategy());
        $this->eventHydrator->addStrategy('groupSlug', new SlugStrategy(new Utf8Slugger()));

        $this->eventHydrator->addStrategy('title', new DescriptionStrategy(new DescriptionCriteria(
            DescriptionStrategy::TITLE,
            null,
            DescriptionStrategy::EVENT_SUMMERY
        ), new TextFormatter()));

        $this->eventHydrator->addStrategy('summarySlug', new DescriptionStrategy(new DescriptionCriteria(
            DescriptionStrategy::EVENT_SUMMERY,
            null,
            DescriptionStrategy::EOL
        ), new SlugFormatter()));

        /**
         * Todo enable as soon, communi supports Markdown
         * $formatter = new MarkdownFormatter(new HtmlConverter(['strip_tags' => true]));.
         */
        $formatter = new ContentFormatter();
        $descriptionStrategy = new DescriptionStrategy(
            new DescriptionCriteria(
                DescriptionStrategy::EOL,
                2
            ),
            $formatter
        );

        $this->eventHydrator->addStrategy('description', $descriptionStrategy);
    }
}
