<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 07:42.
 */

namespace App\Services\Kool\Event;

use App\Entity\Event;
use App\Services\Interfaces\SourceEventCollection;

/**
 * @codeCoverageIgnore
 * Class KoolEventCollection.
 */
class KoolEventCollection implements SourceEventCollection
{
    /**
     * @var EventRequestExecutor
     */
    private $eventRequestExecutor;

    /**
     * KoolEventCollection constructor.
     */
    public function __construct(EventRequestExecutor $eventRequestExecutor)
    {
        $this->eventRequestExecutor = $eventRequestExecutor;
    }

    /**
     * @return array|Event[]
     */
    public function getEvents(): array
    {
        return $this->eventRequestExecutor->getEvents();
    }
}
