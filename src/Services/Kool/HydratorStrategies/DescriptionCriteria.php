<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 09.02.19
 * Time: 11:11.
 */

namespace App\Services\Kool\HydratorStrategies;

/**
 * Class DescriptionCriteria.
 */
class DescriptionCriteria
{
    private ?string $startString;
    private int $startOnOccurrence;
    private ?string $endString;
    private int $endOnOccurrence;

    /**
     * DescriptionCriteria constructor.
     */
    public function __construct(
        string $startString,
        int $startOnOccurrence = null,
        string $endString = null,
        int $endOnOccurrence = null
    ) {
        $this->startString = $startString;
        $this->startOnOccurrence = $startOnOccurrence ?: 1;
        $this->endString = $endString;
        $this->endOnOccurrence = $endOnOccurrence ?: 1;
    }

    public function getStartString(): ?string
    {
        return $this->startString;
    }

    public function getStartOnOccurrence(): int
    {
        return $this->startOnOccurrence;
    }

    public function getEndString(): ?string
    {
        return $this->endString;
    }

    public function getEndOnOccurrence(): int
    {
        return $this->endOnOccurrence;
    }
}
