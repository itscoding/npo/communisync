<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 09.02.19
 * Time: 09:48.
 */

namespace App\Services\Kool\HydratorStrategies;

use App\Services\Kool\HydratorStrategies\Formatter\FormatterInterface;
use Laminas\Hydrator\Strategy\StrategyInterface;

/**
 * Class DescriptionStrategy.
 */
class DescriptionStrategy implements StrategyInterface
{
    const EOL = '<br />';

    const TITLE = 'Titel: ';

    const EVENT_SUMMERY = self::EOL.' Termingruppe: ';

    private DescriptionCriteria $criteria;
    private FormatterInterface $formatter;

    /**
     * DescriptionStrategy constructor.
     */
    public function __construct(
        DescriptionCriteria $criteria,
        FormatterInterface $formatter = null
    ) {
        $this->criteria = $criteria;
        $this->formatter = $formatter;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param mixed $value
     *
     * @return mixed|string
     */
    public function extract($value, ?object $object = null)
    {
        return 'Todo implement method when needed!';
    }

    /**
     * @param mixed $text
     *
     * @return mixed|string
     */
    public function hydrate($text, ?array $data)
    {
        $start = self::findNth(
            $this->criteria->getStartOnOccurrence(),
            $text,
            $this->criteria->getStartString()
        );

        $start = $start + strlen($this->criteria->getStartString());

        if (!$this->criteria->getEndString()) {
            $output = $this->prepareOutput($text, $start);
            if ($this->formatter) {
                $output = $this->formatter->format($output); //@codeCoverageIgnore
            }

            return trim($output);
        }

        $end = self::findNth(
            $this->criteria->getEndOnOccurrence(),
            $text,
            $this->criteria->getEndString(),
            $start
        );

        $output = $this->prepareOutput($text, $start, $end);

        if ($this->formatter) {
            $output = $this->formatter->format($output); //@codeCoverageIgnore
        }

        return trim($output);
    }

    /**
     * @param $start
     * @param null $end
     */
    private function prepareOutput(string $text, int $start, $end = null): string
    {
        if (null === $end) {
            $output = substr($text, $start);
        } else {
            $end = $end - $start;
            $output = substr($text, $start, $end);
        }

        if (substr_count($text, $this->criteria->getStartString()) < $this->criteria->getStartOnOccurrence()) {
            return '';
        }

        return trim($output);
    }

    public static function findNth(int $nth, string $haystack, string $needle, int $initalOffset = 0): ?int
    {
        if ($initalOffset > strlen($haystack)) {
            return 0; //@codeCoverageIgnore
        }

        $strPos = strpos($haystack, $needle, $initalOffset);

        if (false === $strPos) {
            return null;
        }

        for ($i = 1; $i < $nth; ++$i) {
            $strPos = strpos($haystack, $needle, $strPos + 1);
        }

        return $strPos;
    }

    public static function decodeBreaks(string $input): string
    {
        return str_replace(PHP_EOL, DescriptionStrategy::EOL, $input);
    }

    public static function encodeBreaks(string $input): string
    {
        return str_replace(DescriptionStrategy::EOL, PHP_EOL, $input);
    }
}
