<?php

namespace App\Services\Kool\HydratorStrategies\Formatter;

/**
 * Class ContentFormatter.
 */
class ContentFormatter extends TextFormatter
{
    protected $linkText = 'Mehr';

    public function format(string $input): string
    {
        $output = str_replace(['[', ']', '*'], '', $input);
        $output = $this->urlToLink($output);

        return parent::format($output);
    }

    /**
     * @return string|string[]|null
     */
    public function urlToLink(string $input)
    {
        $reg_exUrl = "/((http|https)\:\/\/[a-zA-Z0-9\-\.]+(\.[a-zA-Z]{2,3})?(\/[A-Za-z0-9-._~!$&()*+,;=:]*)*)/";
        $input = preg_replace($reg_exUrl, sprintf('(${1})'), $input);

        return $input;
    }
}
