<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 07.04.19
 * Time: 13:01.
 */

namespace App\Services\Kool\HydratorStrategies\Formatter;

/**
 * Class TextFormatter.
 */
class TextFormatter implements FormatterInterface
{
    public function format(string $input): string
    {
        $lineBreaked = str_replace(['<br />', '</li>'], PHP_EOL, $input);
        $linkFixed = str_replace(['[', ']'], '', $lineBreaked);

        $linkFixed = html_entity_decode($linkFixed);

        return strip_tags(trim($linkFixed));
    }
}
