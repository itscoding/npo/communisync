<?php

namespace App\Services\Kool\HydratorStrategies\Formatter;

use App\Services\Slugger\Utf8Slugger;

/**
 * Class SlugFormatter.
 */
class SlugFormatter implements FormatterInterface
{
    /**
     * @var Utf8Slugger
     */
    private $slugger;

    /**
     * SlugFormatter constructor.
     */
    public function __construct(Utf8Slugger $slugger = null)
    {
        $this->slugger = $slugger ?: new Utf8Slugger();
    }

    public function format(string $input): string
    {
        return $this->slugger->slugify($input);
    }
}
