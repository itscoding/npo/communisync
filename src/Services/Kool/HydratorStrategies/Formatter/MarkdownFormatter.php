<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 07.04.19
 * Time: 13:27.
 */

namespace App\Services\Kool\HydratorStrategies\Formatter;

use League\HTMLToMarkdown\HtmlConverter;

/**
 * Class MarkdownFormatter.
 */
class MarkdownFormatter implements FormatterInterface
{
    /**
     * @var HtmlConverter
     */
    private $converter;

    /**
     * MarkdownFormatter constructor.
     */
    public function __construct(HtmlConverter $converter)
    {
        $this->converter = $converter;
    }

    public function format(string $input): string
    {
        //fix links from "Kool" side
        $input = str_replace(['[', ']'], '', $input);
        $input = html_entity_decode($input);
        $output = $this->converter->convert($input);

        return $output;
    }
}
