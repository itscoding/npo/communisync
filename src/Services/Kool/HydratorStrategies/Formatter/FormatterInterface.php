<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 07.04.19
 * Time: 13:02.
 */

namespace App\Services\Kool\HydratorStrategies\Formatter;

/**
 * Interface FormatterInterface.
 */
interface FormatterInterface
{
    public function format(string $input): string;
}
