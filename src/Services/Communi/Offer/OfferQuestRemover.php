<?php

namespace App\Services\Communi\Offer;

class OfferQuestRemover
{
    private GetOfferQuestExecutor $getOfferExecutor;
    private DeleteOffersExecutor $deleteOffersExecutor;

    public function __construct(
        GetOfferQuestExecutor $getOfferExecutor,
        DeleteOffersExecutor $deleteOffersExecutor
    ) {
        $this->getOfferExecutor = $getOfferExecutor;
        $this->deleteOffersExecutor = $deleteOffersExecutor;
    }

    public function removeOffersAndQuests(int $daysAgo, ?bool $official): array
    {
        $offersAndQuests = $this->getOfferExecutor->getOffersQuests($official);

        return $this->deleteOffersExecutor->deleteOfferQuests($offersAndQuests, $daysAgo);
    }
}
