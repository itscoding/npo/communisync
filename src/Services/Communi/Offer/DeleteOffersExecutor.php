<?php

namespace App\Services\Communi\Offer;

use App\Entity\OfferQuest;
use App\Services\Communi\CommuniAppClient;
use Psr\Log\LoggerInterface;

/**
 * Class DeleteOffersExecutor.
 */
class DeleteOffersExecutor
{
    private CommuniAppClient $client;
    private LoggerInterface $logger;

    /**
     * DeleteOffersExecutor constructor.
     */
    public function __construct(
        CommuniAppClient $client,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param OfferQuest[] $offerQuests
     */
    public function deleteOfferQuests(array $offerQuests = [], int $dayAgo)
    {
        $deleteDate = new \DateTime(sprintf('-%s days', $dayAgo));
        $this->logger->debug(sprintf(
            'remove offers created On older then: {%s}',
            $deleteDate->format('d.m.Y h:i')
        ));
        foreach ($offerQuests as $offerQuest) {
            if ($offerQuest->getCreatedOn() < $deleteDate) {
                $deletedOfferQuests[] = $this->executeDeletion($offerQuest);
            }
        }

        return array_filter($deletedOfferQuests ?? []);
    }

    protected function executeDeletion(OfferQuest $offerQuest)
    {
        try {
            $this->client->deleteOfferQuest($offerQuest);
            $this->logger->debug(sprintf(
                'removed offerquest id: {%s}, type {%s} name: {%s} created_on: {%s}',
                $offerQuest->getId(),
                $offerQuest->getType(),
                $offerQuest->getName(),
                $offerQuest->getCreatedOn()->format('d.m.Y H:i')
            ));
        } catch (\Exception $exception) {
            $this->logger->error(sprintf(
                'error when try to remove offer {%s:%s} > %s',
                $offerQuest->getId(),
                $offerQuest->getName(),
                $exception->getMessage()
            ));

            return null;
        }

        return $offerQuest;
    }
}
