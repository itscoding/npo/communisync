<?php

namespace App\Services\Communi\Offer;

use App\Entity\OfferQuest;
use App\Services\Communi\CommuniAppClient;
use Psr\Log\LoggerInterface;

/**
 * Class GetOfferExecutor.
 */
class GetOfferQuestExecutor
{
    private CommuniAppClient $client;
    private LoggerInterface $logger;
    private int $groupId;

    /**
     * GetOfferExecutor constructor.
     */
    public function __construct(
        CommuniAppClient $client,
        LoggerInterface $logger,
        int $groupId
    ) {
        $this->client = $client;
        $this->groupId = $groupId;
        $this->logger = $logger;
    }

    /**
     * @return array|OfferQuest[]
     */
    public function getOffersQuests(?bool $isOfficial): array
    {
        $query['group'] = $this->groupId;
        if (null !== $isOfficial) {
            $query['isOfficial'] = (bool) $isOfficial;
        }

        try {
            $offersResponse = $this->client->getOffers($query);
            $questResponse = $this->client->getQuests($query);
            $offers = json_decode($offersResponse->getBody()->getContents(), true) ?? [];
            $quests = json_decode($questResponse->getBody()->getContents(), true) ?? [];
        } catch (\Exception $exception) {
            $offers = [];
            $quests = [];
            $this->logger->error(sprintf(
                'error when fetching offers to remove: {%s}',
                $exception->getMessage()
            ));
        }

        foreach ($offers as $offer) {
            $offerQuests[] = new OfferQuest($offer, OfferQuest::OFFER_TYPE);
        }
        foreach ($quests as $quest) {
            $offerQuests[] = new OfferQuest($quest, OfferQuest::QUEST_TYPE);
        }

        return $offerQuests ?? [];
    }
}
