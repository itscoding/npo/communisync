<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 19:52.
 */

namespace App\Services\Communi;

use App\Entity\OfferQuest;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * @codeCoverageIgnore
 * Class CommuniAppClient.
 */
class CommuniAppClient
{
    const AUTH_HEADER = 'X-Authorization';
    const COMMUNI_API_URL = 'https://api.communiapp.de';
    const ROUTE_EVENT = '/rest/event';
    const ROUTE_OFFER = '/rest/offer';
    const ROUTE_QUEST = '/rest/quest';
    private Client $client;

    /**
     * CommuniAppClient constructor.
     *
     * @throws \App\Exception\LoginFailedException
     */
    public function __construct(
        string $jwToken,
        Client $client = null
    ) {
        $this->client = $client ?: new Client([
            'base_uri' => self::COMMUNI_API_URL,
            'headers' => [
                self::AUTH_HEADER => sprintf(' Bearer %s', $jwToken),
                'Content-Type' => 'application/json',
            ],
        ]);
    }

    public function getEventById(int $eventId): ResponseInterface
    {
        return $this->client->get(sprintf('%s/%s', self::ROUTE_EVENT, $eventId));
    }

    public function postEvent(array $event): ResponseInterface
    {
        return $this->client->post(self::ROUTE_EVENT, ['body' => json_encode($event)]);
    }

    public function putEvent(array $eventData, int $eventId): ResponseInterface
    {
        return $this->client->put(sprintf('%s/%s', self::ROUTE_EVENT, $eventId), ['body' => json_encode($eventData)]);
    }

    public function deleteEvent(int $eventId): ResponseInterface
    {
        return $this->client->delete(self::ROUTE_EVENT.'/'.$eventId);
    }

    public function getEvents(): ResponseInterface
    {
        return $this->client->get(self::ROUTE_EVENT);
    }

    public function getOffers(array $query = []): ResponseInterface
    {
        return $this->client->get(self::ROUTE_OFFER, [
            'query' => $query,
        ]);
    }

    public function getQuests(array $query = []): ResponseInterface
    {
        return $this->client->get(self::ROUTE_QUEST, [
            'query' => $query,
        ]);
    }

    public function deleteOfferQuest(OfferQuest $offerQuest): ResponseInterface
    {
        if ($offerQuest->isOfferType()) {
            return $this->client->delete(self::ROUTE_OFFER.'/'.$offerQuest->getId());
        }

        return $this->client->delete(self::ROUTE_QUEST.'/'.$offerQuest->getId());
    }
}
