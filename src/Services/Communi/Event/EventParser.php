<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 21:34.
 */

namespace App\Services\Communi\Event;

use App\Entity\Event;
use App\Exception\InvalidDataException;
use App\Hydrators\EventHydrator;
use App\Hydrators\Strategies\DateTimeStrategy;
use Psr\Http\Message\ResponseInterface;
use Zend\Hydrator\NamingStrategy\MapNamingStrategy;

/**
 * Class EventParser.
 */
class EventParser
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * communi api names => local Entity Names.
     *
     * @var array
     */
    public static $nameMapping = [
        'id' => 'communiEventId',
        'titleFormatted' => 'title',
        'locationFormatted' => 'location',
        'descriptionFormatted' => 'description',
    ];

    /**
     * @var EventHydrator
     */
    private $hydrator;

    /**
     * EventParser constructor.
     *
     * @param EventHydrator $hydrator
     */
    public function __construct()
    {
        $this->hydrator = new EventHydrator();
        $this->hydrator->addStrategy('dateTime', new DateTimeStrategy());
        $this->hydrator->addStrategy('endDateTime', new DateTimeStrategy());
        $this->hydrator->addStrategy('mainOrderDate', new DateTimeStrategy());
        $this->hydrator->setNamingStrategy(MapNamingStrategy::createFromHydrationMap(self::$nameMapping));
    }

    /**
     * @return Event[]
     */
    public function parseEvents(ResponseInterface $response): array
    {
        $data = json_decode($response->getBody()->getContents(), true);
        if ($data && array_key_exists('errorMessage', $data)) {
            $message = http_build_query($data);
            throw new InvalidDataException($message);
        }

        foreach ($data as $eventArray) {
            $events[] = $this->parseSingleEvent($eventArray);
        }

        return array_filter($events ?? []);
    }

    public function parseEvent(ResponseInterface $response, Event $event = null): Event
    {
        $data = json_decode($response->getBody()->getContents(), true);
        if (isset($data['errorMessage'])) {
            throw new InvalidDataException(sprintf('found an error: '.$data['errorMessage']));
        }

        return $this->parseSingleEvent($data, $event);
    }

    /**
     * @return Event
     */
    private function parseSingleEvent(?array $data, Event $event = null): ?Event
    {
        if (!$data) {
            return $event;
        }
        if (isset($data['isOfficial'])) {
            $data['isOfficial'] = (bool) $data['isOfficial'] ?? false;
        }
        if (isset($data['id'])) {
            $data['id'] = (int) $data['id'];
        }
        $event = $this->hydrator->hydrate($data, $event ?: new Event());
        //here is the place to validate
        return $event;
    }
}
