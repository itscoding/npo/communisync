<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 21.02.19
 * Time: 14:25.
 */

namespace App\Services\Communi\Event\Filters;

use App\Entity\Event;
use App\Entity\EventType;
use App\Repository\EventTypeRepository;
use App\Services\Interfaces\IFilter;
use App\Services\LogMailer;

/**
 * Todo Test this class
 * Class DateFilter.
 */
class DateFilter implements IFilter
{
    private EventTypeRepository $repository;

    private LogMailer $logMailer;

    /**
     * Default days ahead to post the event.
     */
    const DEFAULT_DAYS_AHEAD = 7;

    /**
     * DateFilter constructor.
     */
    public function __construct(
        EventTypeRepository $repository,
        LogMailer $logMailer
    ) {
        $this->repository = $repository;
        $this->logMailer = $logMailer;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param array|Event[] $events
     *
     * @return array|Event[]
     */
    public function execute(array $events): array
    {
        $types = $this->getSummeryArray();

        /** @var Event $event */
        foreach ($events as $eventKey => $event) {
            $summerySlug = $event->getSummarySlug();

            if (!array_key_exists($summerySlug, $types)) {
                $eventType = $this->createNewEvent($summerySlug);
                $newAddedTypes[] = $eventType;
                $types[$summerySlug] = $eventType;
                unset($events[$eventKey]);
            } else {
                $eventType = $types[$summerySlug];
            }

            //if daysAheadTimestamp + current timestamp < event timestamp, unset the event
            if ($eventType->getDaysAhead() * 3600 * 24 + time() < $event->getDateTime()->getTimestamp()) {
                unset($events[$eventKey]);
            }
        }

        if (isset($newAddedTypes)) {
            $htmlBody = $this->prepareLogMail($newAddedTypes);
            $this->logMailer->send($htmlBody);
        }

        $this->repository->flush();

        return $events;
    }

    /**
     * @return array|EventType[]
     */
    private function getSummeryArray(): array
    {
        $eventTypes = $this->repository->findAll();
        foreach ($eventTypes as $eventType) {
            $newKey = $eventType->getEventGroup();
            $types[$newKey] = $eventType;
        }

        return $types ?? [];
    }

    private function createNewEvent(string $eventGroupSlug): EventType
    {
        $eventType = new EventType();
        $eventType->setDaysAhead(self::DEFAULT_DAYS_AHEAD);
        $eventType->setEventGroup($eventGroupSlug);
        $this->repository->persist($eventType);

        return $eventType;
    }

    /**
     * @param EventType[] $newAddedTypes
     */
    private function prepareLogMail(array $newAddedTypes)
    {
        $messageBody = '<h3>EventTypes wurden hinzugefügt (Diese Events werden erst beim nächsten durchlauf Synchronisiert):</h3><p>';
        foreach ($newAddedTypes as $eventType) {
            $messageBody .= sprintf('{%s} ---  werden {%s} Tage vor dem Startdatum angezeigt</br>',
                $eventType->getEventGroup(),
                $eventType->getDaysAhead()
            );
        }
        $messageBody .= '</p><p><b>Bitte stelle sicher, dass die neuen Events korrekt konfiguriert werden!</b></p>';

        return $messageBody;
    }
}
