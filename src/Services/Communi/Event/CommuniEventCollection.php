<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 07:37.
 */

namespace App\Services\Communi\Event;

use App\Entity\Event;
use App\Services\Interfaces\SourceEventCollection;

/**
 * Class CommuniEventCollection.
 */
class CommuniEventCollection implements SourceEventCollection
{
    /**
     * @var EventRequestExecutor
     */
    private $eventRequestExecutor;

    /**
     * CommuniEventCollection constructor.
     */
    public function __construct(EventRequestExecutor $eventRequestExecutor)
    {
        $this->eventRequestExecutor = $eventRequestExecutor;
    }

    /**
     * @return array|Event[]
     */
    public function getEvents(): array
    {
        return $this->eventRequestExecutor->getEvents();
    }
}
