<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 09.02.19
 * Time: 16:28.
 */

namespace App\Services\Communi\Event\Decisions;

use App\Entity\Event;
use App\Services\Interfaces\IDecision;

/**
 * Class DescriptionFilterDecision.
 */
class DescriptionFilterDecision implements IDecision
{
    private string $filterRegex;

    /**
     * DescriptionFilterDecision constructor.
     */
    public function __construct(
        string $filterRegex
    ) {
        preg_match($filterRegex, '');
        $this->filterRegex = $filterRegex;
    }

    /**
     * Modify Properties.
     */
    public function decide(Event $event): void
    {
        $description = $event->getDescription();
        $desc = preg_replace($this->filterRegex, '', $description);

        if ($desc) {
            $event->setDescription(trim($desc));
        }
    }

    public function isActive(): bool
    {
        return true;
    }
}
