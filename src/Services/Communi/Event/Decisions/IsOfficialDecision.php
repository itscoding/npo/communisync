<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 09:38.
 */

namespace App\Services\Communi\Event\Decisions;

use App\Entity\Event;
use App\Services\Interfaces\IDecision;

/**
 * @codeCoverageIgnore
 * Class IsOfficialDecision.
 */
class IsOfficialDecision implements IDecision
{
    /*
     * @inheritdoc
     */
    public function decide(Event $event): void
    {
        $event->setIsOfficial(true);
    }

    /**
     * @return bool , true when the decision needs to be executed
     */
    public function isActive(): bool
    {
        return true;
    }
}
