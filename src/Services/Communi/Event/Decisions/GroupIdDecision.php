<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 09:41.
 */

namespace App\Services\Communi\Event\Decisions;

use App\Entity\Event;
use App\Services\Interfaces\IDecision;

/**
 * @codeCoverageIgnore
 * Class GroupIdDecision.
 */
class GroupIdDecision implements IDecision
{
    private int $groupId;

    /**
     * GroupIdDecision constructor.
     */
    public function __construct(int $groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * Modify Properties.
     */
    public function decide(Event $event): void
    {
        $event->setGroup($this->groupId);
    }

    /**
     * @return bool , true when the decision needs to be executed
     */
    public function isActive(): bool
    {
        return true;
    }
}
