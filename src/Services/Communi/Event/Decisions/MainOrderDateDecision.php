<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 03.02.19
 * Time: 10:11.
 */

namespace App\Services\Communi\Event\Decisions;

use App\Entity\Event;
use App\Services\Interfaces\IDecision;

/**
 * @codeCoverageIgnore
 * Class MainOrderDateDecision.
 */
class MainOrderDateDecision implements IDecision
{
    /**
     * Modify Properties.
     */
    public function decide(Event $event): void
    {
        $dateTime = $event->getDateTime();
        $mainOrderDate = clone $dateTime;
        $event->setMainOrderDate($mainOrderDate->modify('-7 days'));
    }

    /**
     * @return bool , true when the decision needs to be executed
     */
    public function isActive(): bool
    {
        return false;
    }
}
