<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 09:54.
 */

namespace App\Services\Communi\Event\Decisions;

use App\Entity\Event;
use App\Services\Interfaces\IDecision;
use Psr\Log\LoggerInterface;

/**
 * @codeCoverageIgnore
 * Class PicUrlDecision.
 */
class PicUrlDecision implements IDecision
{
    private string $listUrl;

    private LoggerInterface $logger;

    /**
     * PicUrlDecision constructor.
     */
    public function __construct(
        string $listUrl,
        LoggerInterface $logger
    ) {
        $this->listUrl = $listUrl;
        $this->logger = $logger;
    }

    /**
     * Modify Properties.
     */
    public function decide(Event $event): void
    {
        if ($event->isSynced()) {
            //only send images when its a new event
            return;
        }

        if (false === ($data = @file_get_contents($this->listUrl))) {
            $this->logger->error(sprintf('Image URL %s not found', $this->listUrl));
        }

        $imageSlug = $event->getSlug().'.jpg';
        $images = json_decode($data, true);

        if (is_array($images) && array_key_exists($imageSlug, $images)) {
            $imageUrl = rtrim($this->listUrl, '/').'/'.$imageSlug;
            $event->setPicUrl($imageUrl);
        }
    }

    /**
     * @return bool , true when the decision needs to be executed
     */
    public function isActive(): bool
    {
        return true;
    }
}
