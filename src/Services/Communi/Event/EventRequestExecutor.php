<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 21:33.
 */

namespace App\Services\Communi\Event;

use App\Entity\Event;
use App\Exception\InvalidDataException;
use App\Exception\NotUpdateableException;
use App\Services\Communi\CommuniAppClient;
use Psr\Log\LoggerInterface;

/**
 * Class EventRequestExecutor.
 */
class EventRequestExecutor
{
    private CommuniAppClient $client;
    private EventParser $eventParser;
    private LoggerInterface $logger;

    /**
     * EventExecutor constructor.
     */
    public function __construct(
        CommuniAppClient $client,
        EventParser $eventParser,
        LoggerInterface $logger
    ) {
        $this->client = $client;
        $this->eventParser = $eventParser;
        $this->logger = $logger;
    }

    public function getEventById(int $eventId): ?Event
    {
        $response = $this->client->getEventById($eventId);

        return $this->eventParser->parseEvent($response);
    }

    public function postEvent(Event $event): Event
    {
        $eventData = $event->jsonSerialize();
        $response = $this->client->postEvent($eventData);

        return $this->eventParser->parseEvent($response, $event);
    }

    /**
     * @throws NotUpdateableException
     */
    public function putEvent(Event $event, array $updatedFields = []): ?Event
    {
        if (!($eventId = $event->getCommuniEventId())) {
            throw new NotUpdateableException('the event can not be updated, if no communiEventId is set');
        }

        $eventData = $event->jsonSerialize();

        if ($updatedFields) {
            foreach ($eventData as $key => $value) {
                if (!in_array($key, $updatedFields)) {
                    unset($eventData[$key]);
                }
            }
        }

        $response = $this->client->putEvent($eventData, $eventId);
        try {
            $event = $this->eventParser->parseEvent($response, $event);
        } catch (InvalidDataException $e) {
            $event = null;
            $this->logger->error($e->getMessage());
        }

        return $event;
    }

    public function deleteEvent(int $eventId): bool
    {
        $response = $this->client->getEventById($eventId);
        $event = $this->eventParser->parseEvent($response);
        if ($id = $event->getCommuniEventId()) {
            $response = $this->client->deleteEvent($eventId);
            if (200 === $response->getStatusCode()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array|Event[]
     */
    public function getEvents(): array
    {
        $response = $this->client->getEvents();

        return $this->eventParser->parseEvents($response);
    }
}
