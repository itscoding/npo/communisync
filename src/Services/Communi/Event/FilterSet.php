<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 21.02.19
 * Time: 14:18.
 */

namespace App\Services\Communi\Event;

use App\Entity\Event;
use App\Services\Interfaces\IFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class FilterSet.
 */
class FilterSet
{
    /**
     * @var array|IFilter[]
     */
    private $filters;

    /**
     * DecisionSet constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        //Load all Classes in the Decisions Directory
        $files = Finder::create()->files()->in(__DIR__.'/Filters')->name('*.php');
        foreach ($files as $file) {
            $class = sprintf('%s\%s\%s', __NAMESPACE__, 'Filters', rtrim($file->getFileName(), '.php'));
            $object = $container->get($class);
            $this->addFilter($object);
        }
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function addFilter(IFilter $filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * @param array|Event[] $events
     */
    public function execute(array $events)
    {
        foreach ($this->filters as $filter) {
            $events = $filter->execute($events);
        }

        return $events;
    }
}
