<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 10:15.
 */

namespace App\Services\Communi\Event;

use App\Entity\Event;
use App\Services\Interfaces\IDecision;
use App\Services\Interfaces\IDecisionSet;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

/**
 * @codeCoverageIgnore
 * Class DecisionSet
 */
class DecisionSet implements IDecisionSet
{
    /**
     * DecisionSet constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        //Load all Classes in the Decisions Directory
        $files = Finder::create()->files()->in(__DIR__.'/Decisions')->name('*.php');
        foreach ($files as $file) {
            $class = sprintf('%s\%s\%s', __NAMESPACE__, 'Decisions', rtrim($file->getFileName(), '.php'));
            $object = $container->get($class);
            $this->addDecision($object);
        }
    }

    /**
     * @var array|IDecision[]
     */
    private $decisions = [];

    public function getDecisions(): array
    {
        return $this->decisions;
    }

    public function setDecisions(array $decisions): void
    {
        foreach ($decisions as $decision) {
            $this->addDecision($decision);
        }
    }

    public function addDecision(IDecision $decision)
    {
        $this->decisions[] = $decision;
    }

    public function process(Event $event)
    {
        foreach ($this->decisions as $decision) {
            if ($decision->isActive()) {
                $decision->decide($event);
            }
        }
    }
}
