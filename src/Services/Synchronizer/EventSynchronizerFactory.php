<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 07:43.
 */

namespace App\Services\Synchronizer;

use App\Services\Kool\Event\KoolEventCollection;

/**
 * @codeCoverageIgnore
 * Class EventSynchronizerFactory.
 */
class EventSynchronizerFactory
{
    private KoolEventCollection $koolEventCollection;
    private EventSynchronizer $eventSynchronizer;

    /**
     * EventSynchronizerFactory constructor.
     */
    public function __construct(
        KoolEventCollection $koolEventCollection,
        EventSynchronizer $eventSynchronizer
    ) {
        $this->koolEventCollection = $koolEventCollection;
        $this->eventSynchronizer = $eventSynchronizer;
    }

    public function createSynchronizer(): EventSynchronizer
    {
        $synchronizer = $this->eventSynchronizer;
        $synchronizer->addSourceCollection($this->koolEventCollection);

        return $synchronizer;
    }
}
