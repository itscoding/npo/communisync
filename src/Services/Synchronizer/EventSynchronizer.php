<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 23:29.
 */

namespace App\Services\Synchronizer;

use App\Entity\Event;
use App\Entity\EventSync;
use App\Exception\EventAlreadyExistsException;
use App\Repository\EventSyncRepository;
use App\Services\Communi\Event\EventRequestExecutor;
use App\Services\Communi\Event\FilterSet;
use App\Services\Interfaces\Iynchronizeable;
use App\Services\Interfaces\SourceEventCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Class EventSynchronizer.
 */
class EventSynchronizer implements Iynchronizeable
{
    /**
     * @var SourceEventCollection[]
     */
    private array $sourceCollections;
    private EventSyncRepository $syncRepository;
    private EventRequestExecutor $eventRequestExecutor;
    private int $created = 0;
    private int $updated = 0;
    private LoggerInterface $logger;
    private FilterSet $filterSet;

    /**
     * EventSynchronizer constructor.
     */
    public function __construct(
        EventSyncRepository $syncRepository,
        EventRequestExecutor $eventRequestExecutor,
        LoggerInterface $logger,
        FilterSet $filterSet
    ) {
        $this->syncRepository = $syncRepository;
        $this->eventRequestExecutor = $eventRequestExecutor;
        $this->logger = $logger;
        $this->filterSet = $filterSet;
    }

    /**
     * @codeCoverageIgnore
     *
     * @throws \App\Exception\NotUpdateableException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function synchronize(array $events, ProgressBar $progress = null)
    {
        foreach ($events as $event) {
            $stored = $this->persistEvent($event);
            if ($stored) {
                if ($progress) {
                    $progress->advance();
                }
                $this->syncRepository->flush();
            }
        }
    }

    /**
     * @throws \App\Exception\NotUpdateableException
     * @throws \Doctrine\ORM\ORMException
     */
    public function persistEvent(Event $event): bool
    {
        $eventSync = $this->syncRepository->findBySourceId($event->getSourceEventId());
        if ($eventSync) {
            $eventSync = $this->updateEventWhenETagChanged($eventSync, $event); //@codeCoverageIgnore
        } else {
            try {
                $eventSync = $this->createEventAndFillSync($event);
                //@codeCoverageIgnoreStart
            } catch (EventAlreadyExistsException $exception) {
                return false;
            }
            //@codeCoverageIgnoreEnd
        }
        $this->syncRepository->storeEventSync($eventSync);

        return true;
    }

    /**
     * @throws \App\Exception\NotUpdateableException
     */
    protected function updateEventWhenETagChanged(EventSync $eventSync, Event $event): EventSync
    {
        if ($eventSync->getSourceEventETag() !== $event->getETag()) {
            $eventSync->setSourceEventETag($event->getETag());
            $event->setSourceEventId($event->getSourceEventId());
            $event = $this->eventRequestExecutor->putEvent($event);
            if (!$event) {
                $this->syncRepository->removeByCommuniId($eventSync->getCommuniEventId());
            }
            ++$this->updated;
        }

        return $eventSync;
    }

    protected function createEventAndFillSync(Event $event): EventSync
    {
        $eventSync = new EventSync();
        $eventSync->setSourceEventETag($event->getETag());
        $this->eventRequestExecutor->postEvent($event);
        $eventSync->setSourceEventId($event->getSourceEventId());
        $eventSync->setCommuniEventId($event->getCommuniEventId());
        if ($this->syncRepository->findByCommuniId($event->getCommuniEventId())) {
            $message = 'communi Event has already be created in the communi app, but the id is not stored locally';
            $this->logger->error($message);
            throw new EventAlreadyExistsException($message);
        }

        ++$this->created;

        return $eventSync;
    }

    /**
     * @param Event[] $events
     */
    public function filterInactiveEvents(array $events)
    {
        foreach ($events as $key => $event) {
            if (false == $event->isActive()) {
                unset($events[$key]);
            }
        }

        return $events;
    }

    /**
     * @return SourceEventCollection[]
     */
    public function getSourceCollections(): array
    {
        return $this->sourceCollections;
    }

    public function addSourceCollection(SourceEventCollection $sourceEventCollection)
    {
        $this->sourceCollections[] = $sourceEventCollection;
    }

    /***
     * @return Event[]
     */
    public function getCollectedEvents(): array
    {
        $events = [];
        foreach ($this->sourceCollections as $collection) {
            $events = array_merge($collection->getEvents(), $events);
        }

        $events = $this->filterInactiveEvents($events);
        $events = $this->filterSet->execute($events);

        return $events;
    }

    public function getCreated(): int
    {
        return $this->created;
    }

    public function getUpdated(): int
    {
        return $this->updated;
    }
}
