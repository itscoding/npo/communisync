<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 07:45.
 */

namespace App\Services\SourceEvents;

use App\Entity\Event;
use App\Hydrators\EventHydrator;
use App\Repository\EventSyncRepository;
use App\Services\Interfaces\IDecisionSet;
use Psr\Http\Message\ResponseInterface;

/**
 * Class SourceEventParser.
 */
abstract class SourceEventParser
{
    private EventSyncRepository $syncRepository;
    protected EventHydrator $eventHydrator;
    private IDecisionSet $decisionSet;

    /**
     * SourceEventParser constructor.
     */
    public function __construct(
        EventSyncRepository $syncRepository,
        IDecisionSet $decisionSet
    ) {
        $this->syncRepository = $syncRepository;
        $this->decisionSet = $decisionSet;
    }

    /**
     * @return Event|Event[]|array
     */
    final public function parseResponse(ResponseInterface $response): array
    {
        $this->eventHydrator = new EventHydrator();
        $this->setHydratorStrategies();
        $events = $this->responseToCommuniEvents($response);

        foreach ($events as $key => $event) {
            $this->decisionSet->process($event);
        }

        $events = $this->setSourceIds($events);

        return $events;
    }

    /**
     * @return array|Event[]
     */
    abstract protected function responseToCommuniEvents(ResponseInterface $response): array;

    /**
     * Set the strategies th "$this->hydrator".
     */
    abstract protected function setHydratorStrategies(): void;

    /**
     * @param array|Event[] $communiEvents
     */
    private function setSourceIds(array $communiEvents)
    {
        $eventSyncs = $this->syncRepository->findWhereIdIn(array_keys($communiEvents));
        foreach ($eventSyncs as $sync) {
            $communiEvents[$sync->getSourceEventId()]->setCommuniEventId($sync->getCommuniEventId());
        }

        return array_values($communiEvents);
    }
}
