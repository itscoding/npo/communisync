<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 09.02.19
 * Time: 08:58.
 */

namespace App\Services\Slugger;

/**
 * Class Utf8Slugger.
 */
class Utf8Slugger
{
    private string $separator;

    /**
     * Utf8Slugger constructor.
     *
     * @param null $separator
     */
    public function __construct($separator = null)
    {
        $this->separator = $separator ?: '-';
    }

    /**
     * @param $string
     * @param null $separator
     *
     * @return string|string[]|null
     */
    public function slugify($string, $separator = null)
    {
        $separator = $separator ?: $this->separator;
        $slug = trim(strip_tags($string));
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        $slug = str_replace('@', 'at', $slug);
        $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
        $slug = preg_replace("/[\/_|+ -]+/", $separator, $slug);
        $slug = mb_strtolower(trim($slug, $separator));

        return $slug;
    }
}
