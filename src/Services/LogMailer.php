<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.03.19
 * Time: 21:07.
 */

namespace App\Services;

/**
 * @deprecated replace swiftmailer
 * Class LogMailer
 */
class LogMailer
{
    /** @deprecated  */
    private \Swift_Mailer $mailer;
    private string $sender;
    private array $receivers;

    /**
     * LogMailer constructor.
     */
    public function __construct(
        \Swift_Mailer $mailer,
        string $sender,
        string $receivers
    ) {
        $this->receivers = explode(',', $receivers);
        $this->sender = $sender;
        $this->mailer = $mailer;
    }

    public function send(string $htmlBody)
    {
        $htmlBody .= '</br>From Communisync by <b>Simon Mueller</b>';

        $message = (new \Swift_Message())
            ->setSubject('Communisync - LogMail')
            ->setFrom($this->sender)
            ->setTo($this->receivers)
            ->setBody($htmlBody, 'text/html');

        $this->mailer->send($message);
    }
}
