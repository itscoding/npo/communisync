<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 09:39.
 */

namespace App\Services\Interfaces;

use App\Entity\Event;

/**
 * Interface IDecision.
 */
interface IDecision
{
    /**
     * Modify Properties.
     */
    public function decide(Event $event): void;

    /**
     * @return bool , true when the decision needs to be executed
     */
    public function isActive(): bool;
}
