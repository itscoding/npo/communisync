<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 07:37.
 */

namespace App\Services\Interfaces;

use App\Entity\Event;

/**
 * Interface SourceCollection.
 */
interface SourceEventCollection
{
    /**
     * @return array|Event[]
     */
    public function getEvents(): array;
}
