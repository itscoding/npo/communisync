<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 10:19.
 */

namespace App\Services\Interfaces;

use App\Entity\Event;

/**
 * Class DecisionSet.
 */
interface IDecisionSet
{
    public function getDecisions(): array;

    public function setDecisions(array $decisions): void;

    public function addDecision(IDecision $decision);

    public function process(Event $event);
}
