<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 21.02.19
 * Time: 14:20.
 */

namespace App\Services\Interfaces;

use App\Entity\Event;

/**
 * Interface IFilter.
 */
interface IFilter
{
    /**
     * @param array|Event[] $events
     *
     * @return array|Event[]
     */
    public function execute(array $events): array;
}
