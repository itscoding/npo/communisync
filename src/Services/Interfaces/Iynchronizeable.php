<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 07:51.
 */

namespace App\Services\Interfaces;

/**
 * Class EventSynchronizer.
 */
interface Iynchronizeable
{
    /**
     * @return SourceEventCollection[]
     */
    public function getSourceCollections(): array;

    public function addSourceCollection(SourceEventCollection $sourceEventCollection);
}
