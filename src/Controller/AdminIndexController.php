<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminIndexController.
 */
class AdminIndexController extends AbstractController
{
    /**
     * @Route("/admin",name="admin_index")
     */
    public function loginAction()
    {
        return $this->render('admin/login.html.twig');
    }
}
