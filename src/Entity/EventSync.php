<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventSyncRepository")
 */
class EventSync
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private ?string $sourceEventId = null;
    /**
     * @ORM\Column(type="integer",unique=true)
     */
    private ?int $communiEventId = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $sourceEventETag = null;

    public function getSourceEventId(): ?string
    {
        return $this->sourceEventId;
    }

    public function setSourceEventId(string $sourceEventId): void
    {
        $this->sourceEventId = $sourceEventId;
    }

    public function getCommuniEventId(): ?int
    {
        return $this->communiEventId;
    }

    public function setCommuniEventId(int $communiEventId): void
    {
        $this->communiEventId = $communiEventId;
    }

    public function getSourceEventETag(): ?string
    {
        return $this->sourceEventETag;
    }

    public function setSourceEventETag(string $sourceEventETag): void
    {
        $this->sourceEventETag = $sourceEventETag;
    }
}
