<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 19.03.19
 * Time: 17:22.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EventType.
 *
 * @ORM\Entity(repositoryClass="App\Repository\EventTypeRepository")
 */
class EventType
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private string $eventGroup;

    /**
     * @ORM\Column(type="integer")
     */
    private int $daysAhead;

    public function getEventGroup(): string
    {
        return $this->eventGroup;
    }

    public function setEventGroup(string $eventGroup): void
    {
        $this->eventGroup = $eventGroup;
    }

    public function getDaysAhead(): int
    {
        return $this->daysAhead;
    }

    public function setDaysAhead(int $daysAhead): void
    {
        $this->daysAhead = $daysAhead;
    }
}
