<?php

namespace App\Entity;

/**
 * Class OfferQuest.
 */
class OfferQuest
{
    public const OFFER_TYPE = 'offer';
    public const QUEST_TYPE = 'quest';

    public const OFFER_QUEST_TYPES = [
        self::OFFER_TYPE,
        self::QUEST_TYPE,
    ];

    private int $id;
    private string $name;
    private string $type;
    private \DateTime $createdOn;

    public function __construct(array $data, string $type)
    {
        $this->exchangeArray($data, $type);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCreatedOn(): \DateTime
    {
        return $this->createdOn;
    }

    public function exchangeArray(array $data, string $type)
    {
        if (!in_array($type, self::OFFER_QUEST_TYPES)) {
            throw new \Exception(sprintf('type {%s} not exists. allowed types are {%s}', $type, explode(',', self::OFFER_QUEST_TYPES)));
        }
        $date = $data['createdOn'] ?? 'now';
        $this->createdOn = new \DateTime($date);
        $this->id = $data['id'];
        $this->type = $type;
        if (self::QUEST_TYPE === $type) {
            $this->name = $data['offerFormatted'];

            return;
        }
        if (self::OFFER_TYPE === $type) {
            $this->name = $data['questFormatted'];

            return;
        }
    }

    public function isOfferType(): bool
    {
        return self::OFFER_TYPE === $this->type;
    }
}
