<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 20:02.
 */

namespace App\Entity;

use App\Services\Communi\Event\EventParser;

/**
 * Class Event.
 */
class Event implements \JsonSerializable
{
    private string $sourceEventId;
    private ?int $communiEventId = null;
    private ?\DateTime $dateTime = null;
    private ?\DateTime $endDateTime = null;
    private bool $isOfficial = false;
    private string $picUrl = '';
    private ?int $group = null;
    private string $title = '';
    private string $location = '';
    private string $description = '';
    private ?int $creatorId = null;
    private ?\DateTime $mainOrderDate = null;
    private bool $isActive = true;
    private string $summary = '';
    private string $summarySlug = '';
    private string $slug = '';

    public function getSourceEventId(): string
    {
        return $this->sourceEventId;
    }

    public function setSourceEventId(string $sourceEventId): void
    {
        $this->sourceEventId = $sourceEventId;
    }

    public function getCommuniEventId(): ?int
    {
        return $this->communiEventId;
    }

    public function setCommuniEventId(int $communiEventId): void
    {
        $this->communiEventId = $communiEventId;
    }

    public function getETag(): string
    {
        $data = $this->jsonSerialize();
        unset($data['picUrl']);

        return md5(json_encode($data));
    }

    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTime $dateTime): void
    {
        $this->dateTime = $dateTime;
    }

    public function getEndDateTime(): ?\DateTime
    {
        return $this->endDateTime;
    }

    public function setEndDateTime(\DateTime $endDateTime): void
    {
        $this->endDateTime = $endDateTime;
    }

    public function isOfficial(): bool
    {
        return $this->isOfficial ?: false;
    }

    public function setIsOfficial(bool $isOfficial): void
    {
        $this->isOfficial = $isOfficial;
    }

    public function getPicUrl(): string
    {
        return $this->picUrl ?: '';
    }

    public function setPicUrl(string $picUrl): void
    {
        if (filter_var($picUrl, FILTER_VALIDATE_URL)) {
            $this->picUrl = $picUrl;
        }
    }

    public function getGroup(): ?int
    {
        return $this->group;
    }

    public function setGroup(int $group): void
    {
        $this->group = $group;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getLocation(): string
    {
        return $this->location ?: '';
    }

    public function setLocation(?string $location): void
    {
        $this->location = $location ?: '';
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getCreatorId(): int
    {
        return $this->creatorId;
    }

    public function setCreatorId(int $creatorId): void
    {
        $this->creatorId = $creatorId;
    }

    /**
     * @return \DateTime
     */
    public function getMainOrderDate(): ?\DateTime
    {
        return $this->mainOrderDate;
    }

    public function setMainOrderDate(\DateTime $mainOrderDate): void
    {
        $this->mainOrderDate = $mainOrderDate;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getSummary(): string
    {
        return $this->summary ?: '';
    }

    public function setSummary(string $summary): void
    {
        $this->summary = $summary;
    }

    public function getSummarySlug(): string
    {
        return $this->summarySlug ?: '';
    }

    public function setSummarySlug(string $summerySlug): void
    {
        $this->summarySlug = $summerySlug;
    }

    public function getSlug(): string
    {
        return $this->slug ?: '';
    }

    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    public function isSynced(): bool
    {
        return $this->getCommuniEventId() && $this->getSourceEventId();
    }

    /**
     * this is the communiapp json representation.
     */
    public function jsonSerialize()
    {
        return [
            'isOfficial' => $this->isOfficial,
            'group' => $this->group,
            'title' => $this->title,
            'description' => $this->description,
            'location' => $this->location,
            'dateTime' => $this->dateTime ? $this->dateTime->format(EventParser::DATE_FORMAT) : null,
            'endDateTime' => $this->endDateTime ? $this->endDateTime->format(EventParser::DATE_FORMAT) : null,
            'picUrl' => $this->picUrl ?: '',
            'mainOrderDate' => $this->mainOrderDate ? $this->mainOrderDate->format(EventParser::DATE_FORMAT) : null,
        ];
    }
}
