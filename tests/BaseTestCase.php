<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 30.01.19
 * Time: 19:21.
 */

namespace App\Tests;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

/**
 * Class BaseTestCase.
 */
class BaseTestCase extends TestCase
{
    /**
     * @return false|string
     */
    public function getTestContent(string $path)
    {
        $file = __DIR__.'/TestData/'.ltrim($path);
        if (file_exists($file)) {
            return file_get_contents($file);
        }
        throw new \Exception('file{ '.$file.'} no found');
    }

    /**
     * @return Response
     */
    public function createMockResponse(string $responseContent, int $status = 200)
    {
        $response = new Response($status);
        $response->getBody()->write($responseContent);
        $response->getBody()->rewind();

        return $response;
    }

    /**
     * @return Response
     */
    public function createMockResponseFromFile(string $path)
    {
        return $this->createMockResponse($this->getTestContent($path));
    }
}
