<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 01.02.19
 * Time: 23:14.
 */

namespace App\Tests\Entity;

use App\Entity\EventSync;
use App\Tests\BaseTestCase;

/**
 * Class EventSyncTest.
 */
class EventSyncTest extends BaseTestCase
{
    /**
     * @group unit
     */
    public function testBasicSetterAndGetter()
    {
        $eventSync = new EventSync();

        $eventSync->getCommuniEventId();
        $eventSync->getSourceEventId();
        $eventSync->getSourceEventETag();

        $eTag = md5('null');
        $eventSync->setCommuniEventId(1);
        $eventSync->setSourceEventId(234);
        $eventSync->setSourceEventETag($eTag);

        $this->assertSame(1, $eventSync->getCommuniEventId());
        $this->assertSame('234', $eventSync->getSourceEventId());
        $this->assertSame($eTag, $eventSync->getSourceEventETag());
    }
}
