<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 21:28.
 */

namespace App\Tests\Entity;

use App\Entity\Event;
use App\Tests\BaseTestCase;

/**
 * Class EventTest.
 */
class EventTest extends BaseTestCase
{
    /**
     * @group unit
     */
    public function testIfGetETagGeneratesExpectedETags()
    {
        $event = new Event();
        $emptyEventETag = $event->getETag();
        $this->assertNotEmpty($emptyEventETag);
        $event->setCommuniEventId(1234);
        $event->setSourceEventId('kool_1234');
        $this->assertEquals($emptyEventETag, $event->getETag());

        $event->setLocation('Church Oftringen');
        $updatedLocationETag = $event->getETag();
        $this->assertNotEquals($emptyEventETag, $updatedLocationETag);
        $event->setGroup(1234);
        $updatedGroupETag = $event->getETag();
        $this->assertNotEquals($updatedLocationETag, $updatedGroupETag);
        $event->setDescription('Its a funny Event');
        $this->assertNotEquals($updatedGroupETag, $event->getETag());
        $this->assertTrue($event->isActive());
        $event->setIsActive(false);
        $this->assertFalse($event->isActive());
    }

    /**
     * most of the methods are tested in other tests (e.g Hydrators).
     *
     * @group unit
     */
    public function testSetterAndGetter()
    {
        $event = new Event();
        $event->setPicUrl('url');
        $this->assertEmpty($event->getPicUrl());
        $event->setPicUrl('http://image.jpg');
        $this->assertEquals('http://image.jpg', $event->getPicUrl());
    }
}
