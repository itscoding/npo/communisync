<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 21:49.
 */

namespace App\Tests\Hydrators\Strategies;

use App\Entity\Event;
use App\Hydrators\Strategies\DateTimeStrategy;
use App\Tests\BaseTestCase;

/**
 * Class DateTimeStrategyTest.
 */
class DateTimeStrategyTest extends BaseTestCase
{
    /**
     * @group unit
     */
    public function testIfExtractCandHandleObjectAndArray()
    {
        $dateTimeStrategy = new DateTimeStrategy();
        $dateTime = $dateTimeStrategy->extract('21.12.2018 10:28:00');
        $this->assertEquals('21.12.2018 10:28:00', $dateTime);
        $event = new Event();
        $event->setDateTime(new \DateTime('02.01.1988 00:45:00'));
        $dateTimeString = $dateTimeStrategy->extract('', $event);
        $this->assertEquals('1988-01-02 00:45:00', $dateTimeString);
    }

    /**
     * @group unit
     */
    public function testIfHydrateCreatesNewDateTimeObject()
    {
        $dateTimeStrategy = new DateTimeStrategy('Europe/Zurich');
        $dateTime = $dateTimeStrategy->hydrate('09.01.1987 18:00', []);
        $this->assertInstanceOf(\DateTime::class, $dateTime);
        $this->assertEquals('09.01.1987 19:00', $dateTime->format('d.m.Y H:i'));
    }
}
