<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 01.02.19
 * Time: 22:44.
 */

namespace App\Tests\Hydrators\Strategies;

use App\Entity\Event;
use App\Hydrators\Strategies\HtmlEncodeStrategy;
use App\Tests\BaseTestCase;

/**
 * Class HtmlEncodeStrategyTest.
 */
class HtmlEncodeStrategyTest extends BaseTestCase
{
    /**
     * @group unit
     */
    public function testIfHydrateDecodesHtml()
    {
        $htmlEncodeStrategy = new HtmlEncodeStrategy();
        $html = $htmlEncodeStrategy->hydrate('Susi &amp; Strolch', []);
        $this->assertEquals('Susi & Strolch', $html);
    }

    /**
     * @group unit
     */
    public function testIfExtractEncodeHtml()
    {
        $htmlEncodeStrategy = new HtmlEncodeStrategy();
        $html = $htmlEncodeStrategy->extract('Susi & — – Strolch');
        $this->assertEquals('Susi &amp; &mdash; &ndash; Strolch', $html);
        $html = $htmlEncodeStrategy->extract('', new Event());
        $this->assertEquals('could not be hydrated', $html);
    }
}
