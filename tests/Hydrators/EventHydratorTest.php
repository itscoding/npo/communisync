<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 31.01.19
 * Time: 21:49.
 */

namespace App\Tests\Hydrators;

use App\Entity\Event;
use App\Hydrators\EventHydrator;
use App\Tests\BaseTestCase;

/**
 * Class EventHydratorTest.
 */
class EventHydratorTest extends BaseTestCase
{
    /**
     * @group unit
     */
    public function testIfEventHydratorReturnsFilledEntity()
    {
        $hydrator = new EventHydrator();
        $event = $hydrator->hydrate([
            'isOfficial' => true,
            'dateTime' => new \DateTime('-1 day'),
            'endDateTime' => new \DateTime('+1 day'),
        ], new Event());

        $this->assertTrue($event->isOfficial());
        $this->assertInstanceOf(\DateTime::class, $event->getDateTime());
        $this->assertEquals(2, date_diff($event->getDateTime(), $event->getEndDateTime())->days);
    }

    /**
     * @group unit
     */
    public function testIfHydratorReturnsFilledArray()
    {
        $hydrator = new EventHydrator();
        $event = new Event();
        $event->setSummary('Summery');
        $event->setTitle('A cool Title');
        $event->setDescription('A event description');
        $event->setSummarySlug('summery');
        $event->setSourceEventId('appId');
        $event->setCreatorId(1);
        $event->setDateTime(new \DateTime('now'));
        $event->setGroup(1);
        $eventArray = $hydrator->extract($event);
        $this->assertArrayHasKey('description', $eventArray);
    }
}
