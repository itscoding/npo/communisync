<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 13:53.
 */

namespace App\Tests\Services\Kool;

use App\Entity\Event;
use App\Entity\EventSync;
use App\Repository\EventSyncRepository;
use App\Services\Communi\Event\DecisionSet;
use App\Services\Kool\Event\EventParser;
use App\Tests\BaseTestCase;
use ICal\ICal;

/**
 * Class EventParserTest.
 */
class EventParserTest extends BaseTestCase
{
    public function createEventParser(): EventParser
    {
        $syncRepo = $this->createMock(EventSyncRepository::class);
        $eventSync = new EventSync();
        $eventSync->setSourceEventId('e2750@kool.kirche-oftringen.ch');
        $eventSync->setCommuniEventId(1);
        $syncRepo->method('findWhereIdIn')->willReturn([$eventSync]);
        $decisionSet = $this->createMock(DecisionSet::class);

        return new EventParser($syncRepo, $decisionSet);
    }

    /**
     * @group unit
     */
    public function testIfResponseToCommuniEventWillReturnEvents()
    {
        $parser = $this->createEventParser();
        $parser->setFilterDaysBefore('1549700347');
        $parser->setFilterDaysAfter('1549700328');
        $iCal = $this->createMock(ICal::class);
        $iCal->method('events')->willReturn([(object) [
            'uid' => 'e2750@kool.kirche-oftringen.ch',
        ]]);
        $response = $this->createMockResponseFromFile('Kool/icalResponse.ics');
        $parser->parseResponse($response);
        $parser->setICal($iCal);
        $parsedResponse = $parser->parseResponse($response);
        foreach ($parsedResponse as $event) {
            $this->assertInstanceOf(Event::class, $event);
        }
    }
}
