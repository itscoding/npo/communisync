<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 09.02.19
 * Time: 09:46.
 */

namespace App\Tests\Services\Kool\HydratorStrategies;

use App\Entity\Event;
use App\Hydrators\EventHydrator;
use App\Services\Kool\HydratorStrategies\DescriptionCriteria;
use App\Services\Kool\HydratorStrategies\DescriptionStrategy;
use App\Services\Kool\HydratorStrategies\Formatter\MarkdownFormatter;
use App\Services\Kool\HydratorStrategies\Formatter\TextFormatter;
use App\Tests\BaseTestCase;
use League\HTMLToMarkdown\HtmlConverter;

/***
 * Class DescriptionStrategyTest
 * @package App\Tests\Services\Kool
 */
class DescriptionStrategyTest extends BaseTestCase
{
    /**
     * @group unit
     *
     * @dataProvider descriptionProvider
     */
    public function testIfKoolEventInputIsHydratedToExpectedCommuniEvent(
        string $description,
        string $expectedTitle,
        string $expectedSummary,
        string $expectedDescription,
        string $eolChar = '<br />'
    ) {
        $hydrator = new EventHydrator();

        $hydrator->addStrategy('title', new DescriptionStrategy(
            new DescriptionCriteria('Titel: ', 1, $eolChar.' Termingruppe:'),
            new TextFormatter()
        ));
        $hydrator->addStrategy('description', new DescriptionStrategy(
            new DescriptionCriteria($eolChar, 3),
            new MarkdownFormatter(new HtmlConverter(['strip_tags' => true]))
        ));

        $hydrator->addStrategy('summary', new DescriptionStrategy(
            new DescriptionCriteria($eolChar.' Termingruppe:', 1, $eolChar),
            new TextFormatter()
        ));

        $event = $hydrator->hydrate([
            'description' => $description,
            'title' => $description,
            'summary' => $description,
        ], new Event());

        $this->assertSame($expectedDescription, $event->getDescription(), 'descripton does not match');
        $this->assertSame($expectedTitle, $event->getTitle(), 'title does not match');
        $this->assertSame($expectedSummary, $event->getSummary(), 'summery does not match');
    }

    public function descriptionProvider(): array
    {
        return [
            [
                'FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//E"><HTML><BODY><br />Titel: @life Jugendgruppe<br /> Termingruppe: @life Jugendgruppe<br /><ul><li>Snacks & Smalltalk</li><li>Worship </li><li>Input</li><li>Chille</li><li>Gemeinschaft</li></ul></BODY></HTML>',
                '@life Jugendgruppe',
                '@life Jugendgruppe',
                '- Snacks &amp; Smalltalk'.PHP_EOL.
                '- Worship'.PHP_EOL.
                '- Input'.PHP_EOL.
                '- Chille'.PHP_EOL.
                '- Gemeinschaft',
            ],
            [
                'FMTTYPE = text / html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//E" ><HTML ><BODY ><br />Titel: Gemeinschaftstreff <br /> Termingruppe: Gemeinschaftstreff </BODY ></HTML > ',
                'Gemeinschaftstreff',
                'Gemeinschaftstreff',
                '',
            ],
        ];
    }
}
