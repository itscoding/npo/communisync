<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 07.02.19
 * Time: 21:47.
 */

namespace App\Tests\Services\Synchronizer;

use App\Entity\Event;
use App\Entity\EventSync;
use App\Exception\EventAlreadyExistsException;
use App\Repository\EventSyncRepository;
use App\Services\Communi\Event\EventRequestExecutor;
use App\Services\Communi\Event\FilterSet;
use App\Services\Kool\Event\EventRequestExecutor as KoolEventRequestExecutor;
use App\Services\Kool\Event\KoolEventCollection;
use App\Services\Synchronizer\EventSynchronizer;
use App\Tests\BaseTestCase;
use Psr\Log\LoggerInterface;

/**
 * Class EventSynchronizerTest.
 */
class EventSynchronizerTest extends BaseTestCase
{
    public function createEventSynchronizer(
        EventSyncRepository $repository = null,
        EventRequestExecutor $eventRequestExecutor = null,
        FilterSet $filterSet = null
    ): EventSynchronizerTestDouble {
        $syncRepository = $repository ?: $this->createMock(EventSyncRepository::class);
        $requestExecutor = $eventRequestExecutor ?: $this->createMock(EventRequestExecutor::class);
        $logger = $this->createMock(LoggerInterface::class);
        $filterSet = $filterSet ?: $this->createMock(FilterSet::class);

        return new EventSynchronizerTestDouble(
            $syncRepository,
            $requestExecutor,
            $logger,
            $filterSet
        );
    }

    /**
     * @group unit
     */
    public function testUpdateEventWhenETagChangedUpdatesTheEvent()
    {
        $eventSync = new EventSync();
        $eventSync->setSourceEventId(1);
        $eventSync->setCommuniEventId(2);
        $event = new Event();
        $event->setSourceEventId(1);
        $event->setCommuniEventId(2);
        $synchronizer = $this->createEventSynchronizer();
        $updatedSync = $synchronizer->updateEventWhenETagChanged($eventSync, $event);
        $this->assertInstanceOf(EventSync::class, $updatedSync);
        $this->assertEquals('cd1acf49c46592da07b28f22c1fff0c8', $updatedSync->getSourceEventETag());
        $this->assertSame(1, $synchronizer->getUpdated());
        $this->assertSame(0, $synchronizer->getCreated());
    }

    /**
     * @group unit
     */
    public function testIfCreateEventAndFillSyncCreatesEvent()
    {
        $executor = $this->createMock(EventRequestExecutor::class);
        $repository = $this->createMock(EventSyncRepository::class);
        $synchronizer = $this->createEventSynchronizer(
            $repository,
            $executor
        );
        $event = new Event();
        $event->setTitle('hello');
        $event->setSourceEventId(1);
        $event->setCommuniEventId(23);
        $createdEventSync = $synchronizer->createEventAndFillSync($event);
        $this->assertInstanceOf(EventSync::class, $createdEventSync);
        $this->assertEquals('b7b45c3949d85b727afd211ae95205c4', $createdEventSync->getSourceEventETag());
        $this->assertSame(0, $synchronizer->getUpdated());
        $this->assertSame(1, $synchronizer->getCreated());

        //Check if the Exception will be thrown
        $repository->method('findByCommuniId')->willreturn(new EventSync());
        $this->expectException(EventAlreadyExistsException::class);
        $synchronizer->createEventAndFillSync($event);
    }

    /**
     * @group unit
     */
    public function testCollectionGettersAndSetters()
    {
        $filterSet = $this->createMock(FilterSet::class);
        $event = new Event();
        $filterSet->method('execute')->willReturn([$event]);
        $synchronizer = $this->createEventSynchronizer(null, null, $filterSet);
        $executor = $this->createMock(KoolEventRequestExecutor::class);
        $executor->method('getEvents')->willReturn([]);
        $collection = new KoolEventCollection($executor);
        $synchronizer->addSourceCollection($collection);
        $collectedEvent = $synchronizer->getCollectedEvents();
        $this->assertCount(1, $collectedEvent);
        $this->assertSame($collectedEvent[0], $event);

        $this->assertSame($synchronizer->getSourceCollections(), [$collection]);
    }

    /**
     * @group unit
     *
     * @dataProvider eventProvider
     */
    public function testIfFilterInactiveEventsRemovesInactiveEventsFromArray(array $events, int $expectedCount)
    {
        $synchronizer = $this->createEventSynchronizer();
        $filtered = $synchronizer->filterInactiveEvents($events);
        $this->assertCount($expectedCount, $filtered);
    }

    public function eventProvider(): array
    {
        $eventOne = new Event();
        $eventOne->setIsActive(false);
        $eventTwo = new Event();
        $eventThree = new Event();
        $eventThree->setIsActive(false);
        $eventFour = new Event();

        return [
            [[$eventOne, $eventTwo], 1],
            [[$eventOne, $eventTwo, $eventThree], 1],
            [[$eventOne, $eventThree, $eventFour], 1],
            [[$eventOne, $eventTwo, $eventThree, $eventFour], 2],
        ];
    }

    /**
     * @group unit
     */
    public function testIfPersistEventsStoresEventSyncsAndPostEventsToCommuni()
    {
        $repo = $this->createMock(EventSyncRepository::class);
        $repo->method('findBySourceId')->willReturn(new EventSync());
        $synchronizer = $this->createEventSynchronizer();
        $event = new Event();
        $event->setSourceEventId(1);
        $event->setCommuniEventId(23);
        $isStored = $synchronizer->persistEvent($event);
        $this->assertTrue($isStored);
    }
}

/**
 * TestDouble.
 */
class EventSynchronizerTestDouble extends EventSynchronizer
{
    /**
     * protected method to test.
     */
    public function updateEventWhenETagChanged(EventSync $eventSync, Event $event): EventSync
    {
        return parent::updateEventWhenETagChanged($eventSync, $event);
    }

    /**
     * protected method to test.
     */
    public function createEventAndFillSync(Event $event): EventSync
    {
        return parent::createEventAndFillSync($event);
    }
}
