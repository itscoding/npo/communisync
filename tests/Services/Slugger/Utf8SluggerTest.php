<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 09.02.19
 * Time: 09:00.
 */

namespace App\Tests\Services\Slugger;

use App\Services\Slugger\Utf8Slugger;
use App\Tests\BaseTestCase;

class Utf8SluggerTest extends BaseTestCase
{
    /**
     * @group unit
     * @dataProvider slugs
     */
    public function testSlugger(string $input, string $expectedSlug)
    {
        $slugger = new Utf8Slugger();
        $slug = $slugger->slugify($input);
        $this->assertSame($expectedSlug, $slug);
    }

    public function slugs(): array
    {
        return [
            ['Spiis & gwand', 'spiis-gwand'],
            ['60+ Nachmittag', '60-nachmittag'],
            ['@ults-Abend-Gottesdienst', 'atults-abend-gottesdienst'],
        ];
    }
}
