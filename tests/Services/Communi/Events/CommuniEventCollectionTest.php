<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 01.02.19
 * Time: 23:15.
 */

namespace App\Tests\Services\Communi\Events;

use App\Entity\Event;
use App\Services\Communi\Event\CommuniEventCollection;
use App\Services\Communi\Event\EventRequestExecutor;
use App\Tests\BaseTestCase;

/**
 * Class CommuniEventCollectionTest.
 */
class CommuniEventCollectionTest extends BaseTestCase
{
    /**
     * @group unit
     * @dataProvider eventTestProvider
     *
     * @param int $counts
     */
    public function testIfCommuniCollectionReturnsArrayOfEvents(array $events, int $count)
    {
        $requester = $this->createMock(EventRequestExecutor::class);
        $requester->method('getEvents')->willReturn($events);
        $communiEventColleciton = new CommuniEventCollection($requester);
        $returnedEvents = $communiEventColleciton->getEvents();
        $this->assertEquals($events, $returnedEvents);
        $this->assertCount($count, $events);
    }

    public function eventTestProvider(): array
    {
        return [
            [[new Event()], 1],
            [[new Event(), new Event(), new Event()], 3],
            [[], 0],
        ];
    }
}
