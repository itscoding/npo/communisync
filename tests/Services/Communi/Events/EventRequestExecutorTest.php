<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 12:41.
 */

namespace App\Tests\Services\Communi\Events;

use App\Entity\Event;
use App\Exception\InvalidDataException;
use App\Exception\NotUpdateableException;
use App\Services\Communi\CommuniAppClient;
use App\Services\Communi\Event\EventParser;
use App\Services\Communi\Event\EventRequestExecutor;
use App\Tests\BaseTestCase;
use Psr\Log\LoggerInterface;

/**
 * Class EventRequestExecutor.
 */
class EventRequestExecutorTest extends BaseTestCase
{
    public function createEventRequestExecutor(Event $event = null): EventRequestExecutor
    {
        $client = $this->createMock(CommuniAppClient::class);
        $eventParser = $this->createMock(EventParser::class);
        $eventParser->method('parseEvent')->willReturn($event ?: new Event());
        $eventParser->method('parseEvents')->willReturn([new Event()]);
        $logger = $this->createMock(LoggerInterface::class);

        return new EventRequestExecutor($client, $eventParser, $logger);
    }

    /**
     * @group unit
     */
    public function testIfGetEventByIdReturnsAnEventOrNull()
    {
        $requestExecutor = $this->createEventRequestExecutor();
        $event = $requestExecutor->getEventById(1);
        $this->assertInstanceOf(Event::class, $event);
    }

    /**
     * @group unit
     */
    public function testIfPostEventReturnsTheSameObjectLikeThePosted()
    {
        $event = new Event();
        $requestExecutor = $this->createEventRequestExecutor($event);
        $responsedEvent = $requestExecutor->postEvent($event);
        $this->assertSame($event, $responsedEvent);
    }

    /**
     * @group unit
     */
    public function testIfPutEventReturnsAnEvent()
    {
        $event = new Event();
        $event->setCommuniEventId(1);
        $requestExecutor = $this->createEventRequestExecutor($event);
        $responsedEvent = $requestExecutor->putEvent($event);
        $this->assertSame($event, $responsedEvent);
    }

    /**
     * @group unit
     */
    public function testIfExceptionIsThrownOnPutEventWithoutCommuneEventId()
    {
        $event = new Event();
        $requestExecutor = $this->createEventRequestExecutor();
        $this->expectException(NotUpdateableException::class);
        $requestExecutor->putEvent($event);
    }

    /**
     * @group unit
     */
    public function testIfInvalidDataExceptionWillCatchAndReturnNull()
    {
        $event = $this->createMock(Event::class);
        $event->method('getCommuniEventId')->willReturn(123);
        $event->method('jsonSerialize')->willReturn([]);
        $client = $this->createMock(CommuniAppClient::class);
        $eventParser = $this->createMock(EventParser::class);
        $eventParser->method('parseEvent')->willThrowException(new InvalidDataException());
        $logger = $this->createMock(LoggerInterface::class);
        $requestExecutor = new EventRequestExecutor($client, $eventParser, $logger);
        $event = $requestExecutor->putEvent($event);
        $this->assertNull($event);
    }

    /**
     * @group unit
     */
    public function testIfPutOnlyUpdatesNotNullValues()
    {
        $event = new Event();
        $event->setDateTime(new \DateTime('2019-02-02 12:40:31'));
        $event->setCommuniEventId(2);
        $event->setTitle('Title');
        $event->setDescription('Description');
        $client = $this->createMock(CommuniAppClient::class);
        $eventParser = $this->createMock(EventParser::class);
        $eventParser->method('parseEvent')->willReturn($event);
        $client->method('putEvent')->with(['dateTime' => '2019-02-02 12:40:31', 'description' => 'Description', 'endDateTime' => null]);
        $logger = $this->createMock(LoggerInterface::class);
        $requestExecutor = new EventRequestExecutor($client, $eventParser, $logger);
        $responsedEvent = $requestExecutor->putEvent($event, ['dateTime', 'endDateTime', 'description']);

        $this->assertSame($event, $responsedEvent);
    }

    /**
     * @group unit
     */
    public function testIfGetEventsReturnsAnArrayOfEvents()
    {
        $requestExecutor = $this->createEventRequestExecutor();
        $events = $requestExecutor->getEvents();
        $this->assertInstanceOf(Event::class, $events[0]);
    }

    /**
     * @group unit
     */
    public function testIfDeleteReturnsCorrectBoolean()
    {
        $event = new Event();
        $event->setDateTime(new \DateTime('2019-02-02 12:40:31'));
        $event->setCommuniEventId(2);
        $event->setTitle('Title');
        $event->setDescription('Description');
        $client = $this->createMock(CommuniAppClient::class);
        $client->method('deleteEvent')->willReturn(
            $this->createMockResponse('')
        );
        $eventParser = $this->createMock(EventParser::class);
        $eventParser->method('parseEvent')->willReturn($event);
        $logger = $this->createMock(LoggerInterface::class);
        $requestExecutor = new EventRequestExecutor($client, $eventParser, $logger);
        $isDeleted = $requestExecutor->deleteEvent(2);
        $this->assertEquals(true, $isDeleted);

        $client = $this->createMock(CommuniAppClient::class);
        $requestExecutor = new EventRequestExecutor($client, $eventParser, $logger);
        $client->method('deleteEvent')->willReturn(
            $this->createMockResponse('', 201)
        );
        $isDeleted = $requestExecutor->deleteEvent(1);
        $this->assertEquals(false, $isDeleted);
    }
}
