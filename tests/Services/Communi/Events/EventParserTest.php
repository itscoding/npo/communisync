<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 02.02.19
 * Time: 10:28.
 */

namespace App\Tests\Services\Communi\Events;

use App\Entity\Event;
use App\Exception\InvalidDataException;
use App\Services\Communi\Event\EventParser;
use App\Tests\BaseTestCase;

/**
 * Class EventParserTest.
 */
class EventParserTest extends BaseTestCase
{
    /**
     * @group unit
     */
    public function testIfParseEventsReturnsEventsOnValidResponse()
    {
        $parser = new EventParser();
        $events = $parser->parseEvents($this->createMockResponse('[{},{}]'));
        $this->assertEmpty($events);

        $response = $this->createMockResponse(
            $this->getTestContent('Communi/eventsResponse.json')
        );
        $events = $parser->parseEvents($response);
        $this->assertInstanceOf(Event::class, $events[0]);
    }

    /**
     * @group unit
     */
    public function testIfParseEventsReturnsEventOnValidResponse()
    {
        $parser = new EventParser();
        $response = $this->createMockResponse(
            $this->getTestContent('Communi/eventResponse.json')
        );
        $event = $parser->parseEvent($response);
        $this->assertInstanceOf(Event::class, $event);
    }

    /**
     * @group unit
     */
    public function testIfParseEventsThrowsExceptionOnErrorData()
    {
        $parser = new EventParser();
        $this->expectException(InvalidDataException::class);
        $parser->parseEvents($this->createMockResponse(
            $this->getTestContent('Communi/errorResponse.json')
        ));
    }

    /**
     * @group unit
     */
    public function testIfParseEventThrowsExceptionOnErrorData()
    {
        $parser = new EventParser();
        $this->expectException(InvalidDataException::class);
        $parser->parseEvent($this->createMockResponse(
            $this->getTestContent('Communi/errorResponse.json')
        ));
    }
}
