# Changelog
All notable changes to this project will be documented in this file.


## 2.1.2 - 2022-07-08
### Updated
- Increased filderDaysAfter from 112 to 999 

## 2.0.2 - 2021-03-05
### Fixed
- Fixed url replacement in the description part #11

## 2.0.1 - 2021-02-28
### Changed
- Changed delete Offer to DeleteOfferQuests, so Offer/Quest both types can be deleted
- Changed HTML encoding in the Kool Hydrators, so `&ndash` and `&mdash` will not cause problems
- Added missing return statement in CommuniSyncEventsCommand

## 2.0.0 - 2021-02-28
### Updated
- Updated Symfony to 5.2
### Added
- Added a Command to delete offers older than
### Removed
- Removed --run-dry for json_pp output, instead we just pretty print it to the console

## 1.9.1 - 2019-08-21
### Removed commented code line which avoided the sync job to actually sync

## 1.9.0 - 2019-08-20
### Removed
- removed mainOrderDate "decision", the mainOrderDate will no logner be set 
### Added
- added --dry-run=1 option `./bin/console communi:sync:events --dry-run=1 | json_pp` will output the json of the events, whitout pushing them to the api


## 1.8.0 - 2019-07-05
### Added
- Added Contentformatter, to Format the Kool Descirpitons of the Events

### Updated
- Changed From Markdown to ContentFormatter, for the contents

## 1.7.0 - 2019-05-28
### Fixed
- Resolved a Problem with wrong Date-Format from the Kool Events

### Changed
- The SummerySlug is now created from "Termingruppe" and not from "Summary" anymore

## 1.6.3 - 2019-04-25
### Fixed
- The Slug in the eventType is no more from the Title but from the Summary
- Updated Event to pass all tests

## 1.6.2 - 2019-04-16
### Added
- Added LocaleListener, the slugger now replaces ä with ae ö with oe and ü with ue. 

## 1.6.1 - 2019-04-16
### Fixed
- Fixed Changing Date because of Date Modification in MainOrderDateDecision

### Fixed
- Fixed some Typos
- Changed default days ahead from 42 to 7 to avoid too many events

## 1.6.0 - 2019-04-07
### Added
- Added a Markdown Formatter to format the Description before pushin to communi
- Added the `--dry-file` option, to execute the SyncCommand without pushing data to communi, but save to a file

## 1.5.0-RC - 2019-03-30
### Updated
- Updated Version to be the Release Candidate
- Format HTML

## 1.5.0-beta - 2019-03-21
### Fixed
- Fixed some minor Issues
### Updated
- Updated Parser, after Kool Changes the iCal Output

### Added
- Added EventType Entity and Repository
- Added an Event Filter who Filters all Events that not should be Synced 
- Added DateFilter to All Events out that too far in the future (by criteria)  
- Added LogMailer Class
- Added LogMailer To the DateFilter, to send Informations about unconfigured EventTypes

### Breaking Changes
- Added SwiftMailerBundle to composer Dependencies, composer update or install is needed

## 1.4.0-beta - 2019-02-21
### Added
- Added A Parser, to Parse the event Title, Description and Summary #5
- Added the Event Image Service Decision #6
- Added Boilerplate for Event Date Filter service #9
- Added Code to Filter sentences and words in the description #7 
- Added Event Filter 

## 1.3.0-beta - 2019-02-07
### Added
- Added some tests to Test the communi Services #2
- Added some tests to Test the Sync Service #2 

## 1.2.0-Beta - 2019-02-06
### Fixed
- Fixed some issues with Excetipions while create/update/delete stuff on the communi api
### Added
- Implemented basic errorhandling, so the tasks not abortet after an error
### Removed
- Removed Flickr Implementation

## 1.1.0-Beta - 2019-02-05
### Added
- Added delete command to delete already synced events
- Added tests #2

## 1.0.0-Beta - 2019-02-03
### Changed
- Updated Readme
### Updated
- Updated config 
 ### Added
- Added MIT LICENCE File
- Added `mainOrderDate` property to the `Event` Entity and created `MainOrderDateDecision` 
### Fixed
- Fixed an Issue that causes a Post request the first time when the "put" method is running, event the event had no changes 

## 0.5.0 - 2019-02-02
### Added
- Added Tests for Hydrator Strategies
- Added Entity Tests
- Added Decision's 
- Added Flickr Client (Experimental)
- Added Image Decision's
- Added description to README.md

## 0.4.0 - 2019-02-01
### Added
- Added Kool Client Implementation
- Added Options for the kOOL client

### Updated
- Updated Sync Class
## 0.3.0 - 2019-01-31
### Added
- Added some Tests

## 0.2.0 - 2019-01-31
### Added
- Added Sync Class

## 0.1.0 - 2019-01-30
### Added
- Added Communiapp Client
- Added Event Entity
- Added Boilerplate Code for kOOL events
