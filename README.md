# Communisync

[![pipeline status](https://gitlab.com/itscoding/npo/communisync/badges/development/pipeline.svg)](https://gitlab.com/itscoding/npo/communisync/commits/development)
[![coverage report](https://gitlab.com/itscoding/npo/communisync/badges/development/coverage.svg)](https://gitlab.com/itscoding/npo/communisync/commits/development)

###### &copy; Simon Müller

[GitLab](https://gitlab.com/boscho87), [GitHub](https://github.com/boscho87)

### Background

Das Tool wurde mit hilfe des [Symfony Frameworks](https://symfony.com/what-is-symfony) umgesetzt.

Bei Fragen, gerne an [Simon D. Müller](simon.d.mueller@gmail.com) wenden.

Falls Sie mich für den Aufwand unterstützen möchten. :-)
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="UPFMS7JWMR2VE" />
<input type="image" src="https://www.paypalobjects.com/de_DE/CH/i/btn/btn_donate_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/de_CH/i/scr/pixel.gif" width="1" height="1" />
</form>

### Funktionalität/Funktionsumfang

Es wird lediglich ein Webserver mit php7.4 und einer Datenbank (mysql/postgres/sqlite) benötigt. Webzugriff ist nicht zwingend notwendig.

#### Kalender

Die Applikation synchronisiert Kalendereinträge aus verschiedenen Tools und spiegelt diese auf die **communiapp**. Die
Anbindung von anderen Kalender (Google/Facebook/etc.) ist mit relativ wenig Aufwand möglich (je nach
authentifizierungsverfahren).

- Bilder werden nur beim erstellen, aber nicht beim Aktualisiern von Events "hochgeladen"
- Es werden *maximal* `20` Events der *nächsten* `28` Tage
  synchronisiert `Todo (parametrisieren) - Soll in Zukunft anpassbar sein, (kommentar prüfen) @SDM 28.02.2021`
- Es werden *keine* Events der Vergangenheit synchronisiert
- Bilder können aus einem Flickr Album geladen werden (Beta Release)

###### Unterstütze Plattformen

- [Communiapp](https://communiapp.de) - ***Sync Ziel***
- [kOOL](http://www.churchtool.org) - The Church Tool (Events)
- [KiKartei](https://kikartei.ch) - KiKartei   (Events)


### Ausführen der Applikation

Es handelt sich um eine Symfony-Applikation, alle commands die nicht mit `communi` beginnen, sind Symfony eigene.

##### Datenbank befehle:

- `php bin/console doctrine:schema:create` - erstellen
- `php bin/console doctrine:schema:drop --force` - löschen

##### Sync Befehle

- `php bin/console communi:sync:events`
- `php bin/console communi:sync:events dry-run`

##### Löschen von Suchen/Bieten in der **Communiapp**

`php bin/console communi:delete:offers (tage-zurück) [official]`

Beispiele:

tage-zurück: muss angegeben werden                                   
`php bin/console communi:delete:offers 25 // löscht "offers" die vor mehr als 25 Tagen erstellt wurden`

official: ist optional (wenn nicht angegeben, löscht es "Offizielle" und nicht "Offizielle" aus der App)

###### löscht "offers" die vor mehr als 29 Tagen erstellt wurden und nicht als "Offiziel markiert wurden

`php bin/console communi:delete:offers 29 0`

###### löscht "offers" die vor mehr als 46 Tagen erstellt wurden und als "Offiziel" markiert wurden

***alles ausser 0 wird als "wahr/true" evaliuiert, also für nicht offizielle immer `0` verwenden***  
`php bin/console communi:delete:offers 46 1`


### Ablauf

1. Events werden von den externen Tools abgeholt und gesammelt.
2. Events werden in ein "Standard" format gebracht.
3. Sogenannte "Decicions" werden ausgeführt, um attribute auf dem Event zu modifizieren
4. Die "Filter" werden ausgeführt, um Events z.B nach Datum zu Filtern
5. Die Events werden an die ***communiapp*** gesendet
6. Ein "Event" hat nun eine `communiId` (aus der Antwort von ***communiapp***) und eine `sourceEventId` diese werden
   zusammen mit einem "EntityTag" `sourceETag` welcher sich nur dann ändert, wenn sich Daten eine Events an der Quelle
   sich verändert haben.
7. Wenn ein Event ein wiederholtes mal abgeholt wird, wird anhand des `sourceETags` geprüft, ob er aktualisiert werden
   muss oder nicht.
8. Falls sich der `sourceETags` vom abgeholten event mit dem `sourceETag` aus der Datenbank unterscheidet, wird die
   Aktualisierung des Events an die Communiapp gesendet, und der neue `sourceETags` wird gespeichert.
   

#### kOOL Spezifisch

Die Veranstaltungen werden normalerweise `28` Tage vor dem Datum an dem Sie Stattfindet synchronisiert. Wenn eine Veranstalungskategorie zum ersten Mal Synchronisiert wird, wird ein Mail generiert, welches darauf hinweist.
Der Wert kann aber in der Datenbank jederzeit angepasst werden.

### Konfigurationsparameter `.env` Datei, oder als "env Variabeln"

#### communiapp parameter

##### `COMMUNI_JSON_WEB_TOKEN` (erforderlich)

Json Web Token, zur Authorisierung der Communiapp rest API

1. Einloggen https://mustergemeinde.communiapp.de/page/login/tab/login
2. https://mustergemeinde.communiapp.de/sync-back/getJwt den wert bei `jwt` kopieren

##### `COMMUNI_EVENT_GROUP_ID` (erforderlich)

Group ID der Communiapp, erfahrt ihr von ***communiapp*** (oder
auf https://mustergemeinde.communiapp.de/rest/event/{existierende_id}) -> den wert group

### Bilder

#### `IMAGE_BASE_URL` (optional)

Dieses PHP Script kann in ein Verzeichniss eines Webserver gelegt werden.

```php
<?php
foreach(glob(__DIR__.'/*.jpg') as $file) {
  $files = explode('/',$file);
  $filename = end($files);
  $schema = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
  $imageUrl = sprintf('%s://%s/%s/%s',$schema,$_SERVER['HTTP_HOST'],trim($_SERVER['REQUEST_URI'],'/'),trim($filename,'/'));
  $files[$filename] = $imageUrl;
}
header('Content-Type: application/json');
echo json_encode($files ?? []);
```

Die Bildernamen müssen dann wie Kategorienamen des Events heissen aber "slugified"

- `Spiis & Gwand` = `spiis-amp-gwand.jpg`
- `Gottesdienst` = `gottesdienst.jpg`
- `@omic Gottesdienst` = `atomic-gottesdienst.jpg`
- `Gottesdienst im Alterszentrum Lindenhof` = `gottesdienst-im-alterszentrum-lindenhof.jpg`

###### Slugger Logik

 ```php
 <?php
 class Utf8Slugger {
     
     public function slugify($string, $separator = null)
     {
         $separator = $separator ?: $this->separator;
         $slug = trim(strip_tags($string));
         $slug = str_replace('@', 'at', $slug);
         $slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
         $slug = preg_replace("/[\/_|+ -]+/", $separator, $slug);
         $slug = strtolower(trim($slug, $separator));
 
         return $slug;
     }    
 }
 ```

### Mails

#### `MAILER_URL` (erforderlich)

[Aus der Symfony Doc](https://symfony.com/doc/current/email.html#configuration)

#### `LOG_MAIL_SENDER` (erforderlich)

- Absender Addresse für LogMails, sollte mit der Konfiguration von `MAILER_URL` übereinstimmen (
  z.B `noreply@commnuisync.ch` -> nur eine Adrese möglich!)

#### `LOG_MAIL_RECEIVER` (erforderlich)

- Komma Separierter string mit Email Empfänger für Log-Mails `test@communisync.ch,admin@communisync.ch`

### Decisions `src/Services/Communi/Event/Decisions`

Hier können klassen Programmiert werden, welche die Attribute der Events Modifizieren. (Als Doku am besten die
bestehenden Klassen anschauen)

### Filter `src/Services/Communi/Event/Filters`

Hier können Filter implementiert werden, welche das Event Array gitert und bestimme events löschen kann (Anhand von
Attributten des/der Events) (Als Doku am besten die bestehenden Klassen anschauen)

## Anbindungen

### kOOL

https://www.churchtool.org/de/home.html

Die Parameter sind momentan erforderlich, das das Tool nur diese Anbindung implementiert hat

##### `KOOL_URL` (erforderlich)

Url des kOOL Dienstes (meistens `https://kool.mustergemeinde.ch`)

##### `KOOL_USER_HASH` (erforderlich)

User `hash` mit Berechtigung auf die iCal (`https://kool.mustergemeinde.ch/ical?user={hash}`)

##### `KOOL_EGS_PARAMETER` (optional)

`egs` Filter Parameter für iCal events (`https://kool.mustergemeinde.ch/ical?user={hash}&egs{egs_parameter}`)
