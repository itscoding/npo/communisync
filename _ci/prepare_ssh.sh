#!/bin/bash
mkdir ~/.ssh
echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
eval "$(ssh-agent -s)"
ssh-add <(echo "$SSH_PRIVATE_KEY")
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
