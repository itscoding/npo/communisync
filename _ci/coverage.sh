#!/bin/bash
./bin/phpunit -c ./phpunit.xml.dist --coverage-html=./test_doc/coverage --testdox-html=./test_doc/index.html --coverage-text --group=unit,database
